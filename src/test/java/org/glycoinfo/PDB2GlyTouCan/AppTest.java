package org.glycoinfo.PDB2GlyTouCan;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.apache.jena.rdf.model.Model;
import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.PdbJsonParser;
import org.glycoinfo.PDB2GlyTouCan.io.file.Files;
import org.glycoinfo.PDB2GlyTouCan.io.file.RemoteChemicalComponentLoader;
import org.glycoinfo.PDB2GlyTouCan.io.model.GlyTouCan;
import org.glycoinfo.PDB2GlyTouCan.io.model.Protein;
import org.glycoinfo.PDB2GlyTouCan.rdf.GlyTouCanModelBuilder;
import org.glycoinfo.PDB2GlyTouCan.structure.glyco.GlycoProtein;
import org.glycoinfo.PDB2GlyTouCan.structure.glyco.ModelBuilder;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.Reader;

import static org.glycoinfo.PDB2GlyTouCan.util.StringUtils.formatTime;

public class AppTest {

  @Test
  public void turtle() {
    try (Reader reader = Files.newBufferedReader(new File("./testdata/2ikc.json"))) {
      long start = System.currentTimeMillis();

      PdbJsonParser parser = new PdbJsonParser(reader);

      Protein protein = parser.parse();
      ModelBuilder builder = new ModelBuilder(protein, new RemoteChemicalComponentLoader());

      GlycoProtein gp = builder.build();
      GlyTouCan json = gp.toJson();

      if (json.getGlyco_entries().isEmpty()) {
        return;
      }

      json.setTime(formatTime(System.currentTimeMillis() - start));
      json.setDate(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));

      GlyTouCanModelBuilder rdfBuilder = new GlyTouCanModelBuilder();

      Model model = rdfBuilder.entry(json).build();

      model.write(System.out, "TURTLE", GlyTouCanModelBuilder.BASE);
    } catch (IOException | MMjsonParseException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void json() {
    try (Reader reader = Files.newBufferedReader(new File("./testdata/2ikc.json"))) {
      long start = System.currentTimeMillis();

      PdbJsonParser parser = new PdbJsonParser(reader);

      Protein protein = parser.parse();
      ModelBuilder builder = new ModelBuilder(protein, new RemoteChemicalComponentLoader());

      GlycoProtein gp = builder.build();
      GlyTouCan json = gp.toJson();

      if (json.getGlyco_entries().isEmpty()) {
        return;
      }

      json.setTime(formatTime(System.currentTimeMillis() - start));
      json.setDate(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));

      ObjectMapper mapper = new ObjectMapper();
      ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());

      writer.writeValue(System.out, json);

    } catch (IOException | MMjsonParseException e) {
      e.printStackTrace();
    }
  }
}
