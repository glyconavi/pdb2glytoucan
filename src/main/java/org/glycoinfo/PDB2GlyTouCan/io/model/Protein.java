package org.glycoinfo.PDB2GlyTouCan.io.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.Entry;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructAsym;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructConn;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructRefSeq;
import org.glycoinfo.PDB2GlyTouCan.structure.Proteindb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Protein {

  private List<Entry> entries;

  private List<AtomSite> atomSites;
  private List<ChemComp> chemComps;
  private List<EntityPoly> entityPolies;
  private List<PdbxStructModResidue> pdbxStructModResidues;
  private List<StructAsym> structAsyms;
  private List<StructConn> structConns;
  private List<StructRefSeq> structRefSeqs;

  public String getPdbCode() {
    if (entries == null) {
      return null;
    } else if (entries.get(0) == null) {
      return null;
    } else {
      return entries.get(0).getId();
    }
  }

  public List<Entry> getEntry() {
    return entries;
  }

  public void setEntry(List<Entry> entries) {
    this.entries = entries;
  }

  @JsonProperty("atom_site")
  public List<AtomSite> getAtomSites() {
    return atomSites;
  }

  public void setAtomSites(List<AtomSite> atomSites) {
    this.atomSites = atomSites;
  }

  @JsonIgnore
  public List<StructAsym> getStructAsyms() {
    return structAsyms;
  }

  public void setStructAsyms(List<StructAsym> structAsyms) {
    this.structAsyms = structAsyms;
  }

  @JsonIgnore
  public List<EntityPoly> getEntityPolies() {
    return entityPolies;
  }

  public void setEntityPolies(List<EntityPoly> entityPolies) {
    this.entityPolies = entityPolies;
  }

  @JsonIgnore
  public List<StructRefSeq> getStructRefSeqs() {
    return structRefSeqs;
  }

  public void setStructRefSeqs(List<StructRefSeq> structRefSeqs) {
    this.structRefSeqs = structRefSeqs;
  }

  @JsonIgnore
  public List<ChemComp> getChemComps() {
    return chemComps;
  }

  public void setChemComps(List<ChemComp> chemComps) {
    this.chemComps = chemComps;
  }

  @JsonIgnore
  public List<PdbxStructModResidue> getPdbxStructModResidues() {
    return pdbxStructModResidues;
  }

  public void setPdbxStructModResidues(List<PdbxStructModResidue> pdbxStructModResidues) {
    this.pdbxStructModResidues = pdbxStructModResidues;
  }

  @JsonIgnore
  public List<StructConn> getStructConns() {
    return structConns;
  }

  public void setStructConns(
      List<StructConn> structConns) {
    this.structConns = structConns;
  }

  public List<PdbxStructModResidue> arPdbxStructModResidue() {
    return pdbxStructModResidues.stream()
        .filter(x -> x.getDetails().equals("GLYCOSYLATION SITE"))
        .collect(Collectors.toList());
  }

  private List<Proteindb> proteinDb;
  private Map<String, Set<String>> connected_asym;
  private List<StructConn> strConnList;

  public List<Proteindb> proteinDb() {
    if (proteinDb != null && !proteinDb.isEmpty()) {
      return proteinDb;
    }

    proteinDb = new ArrayList<>();

    for (StructRefSeq srs : structRefSeqs) {
      Proteindb pd = new Proteindb();

      pd.setId(srs.getPdbx_db_accession());
      pd.setStrand_id(srs.getPdbx_strand_id());

      for (EntityPoly ep : entityPolies) {
        if (ep.getPdbx_strand_id().contains(srs.getPdbx_strand_id())) {
          pd.setEntity_id(ep.getEntity_id());
          pd.setType(ep.getType());
          pd.setPdbx_seq_one_letter_code_can(ep.getPdbx_seq_one_letter_code_can());
        }
      }

      proteinDb.add(pd);
    }

    return proteinDb;
  }

  private void structConn() {
    connected_asym = new HashMap<>();
    strConnList = new LinkedList<>();

    for (StructConn sc : structConns) {
      String s1 = sc.getPtnr1_label_asym_id();
      String s2 = sc.getPtnr2_label_asym_id();

      connected_asym.putIfAbsent(s1, new HashSet<>());
      connected_asym.putIfAbsent(s2, new HashSet<>());

      connected_asym.get(s1).add(s2);
      connected_asym.get(s2).add(s1);
    }

  }

  public Map<String, Set<String>> entityToStructAsym() {
    Map<String, Set<String>> map = new HashMap<>();

    for (StructAsym sa : structAsyms) {
      map.putIfAbsent(sa.getEntity_id(), new HashSet<>());
      map.get(sa.getEntity_id()).add(sa.getId());
    }

    return map;
  }
}
