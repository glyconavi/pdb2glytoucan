package org.glycoinfo.PDB2GlyTouCan.io.model.protein;

public class Entry {

  private String id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
