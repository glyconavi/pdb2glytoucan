package org.glycoinfo.PDB2GlyTouCan.io.model;

import org.biojava.nbio.structure.io.mmcif.model.ChemCompAtom;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompBond;
import org.glycoinfo.PDB2GlyTouCan.structure.Atom;
import org.glycoinfo.PDB2GlyTouCan.structure.Bond;
import org.glycoinfo.PDB2GlyTouCan.structure.Mol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChemicalComponent {

  private static final Map<String, Mol> cache = new HashMap<>();

  private String id;
  private List<ChemCompAtom> chemCompAtoms;
  private List<ChemCompBond> chemCompBonds;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<ChemCompAtom> getChemCompAtoms() {
    return chemCompAtoms;
  }

  public void setChemCompAtoms(List<ChemCompAtom> chemCompAtoms) {
    this.chemCompAtoms = chemCompAtoms;
  }

  public List<ChemCompBond> getChemCompBonds() {
    return chemCompBonds;
  }

  public void setChemCompBonds(List<ChemCompBond> chemCompBonds) {
    this.chemCompBonds = chemCompBonds;
  }

  public Mol toMol() {
    synchronized (cache) {
      if (cache.containsKey(getId())) {
        return cache.get(getId());
      }
    }

    List<Atom> atoms = new ArrayList<>();

    int atom_count = 1;
    for (ChemCompAtom cca : chemCompAtoms) {
      Atom at = new Atom();

      at.setAtom_site_label_atom_id(cca.getAtom_id().replaceAll("\"", ""));
      at.setX(Double.parseDouble(cca.getPdbx_model_Cartn_x_ideal()));
      at.setY(Double.parseDouble(cca.getPdbx_model_Cartn_y_ideal()));
      at.setZ(Double.parseDouble(cca.getPdbx_model_Cartn_z_ideal()));
      at.setAtom_site_type_symbol(cca.getType_symbol());
      at.setChem_comp_atom_charge(Integer.parseInt(cca.getCharge()));
      at.setComp_id(getId().toUpperCase());
      at.setCtfile_id(atom_count);

      atom_count++;

      atoms.add(at);
    }

    List<Bond> bonds = new ArrayList<>();

    for (ChemCompBond ccb : chemCompBonds) {
      Bond bond = new Bond();

      bond.setAromoatic(!ccb.getPdbx_aromatic_flag().equals("N"));

      switch (ccb.getValue_order()) {
        case "SING":
          bond.setBondOrder(1);
          break;
        case "DOUB":
          bond.setBondOrder(2);
          break;
        case "TRIP":
          bond.setBondOrder(3);
          break;
      }

      String atomId1 = ccb.getAtom_id_1().replaceAll("\"", "");

      Atom fromAtom = atoms
          .stream()
          .filter(x -> atomId1.equals(x.getAtom_site_label_atom_id()))
          .findFirst()
          .orElse(null);

      String atomId2 = ccb.getAtom_id_2().replaceAll("\"", "");

      Atom tatom = atoms
          .stream()
          .filter(x -> atomId2.equals(x.getAtom_site_label_atom_id()))
          .findFirst()
          .orElse(null);

      bond.setFromAtom(fromAtom);
      bond.setToAtom(tatom);

      bonds.add(bond);
    }

    Mol mol = new Mol(atoms, bonds);

    synchronized (cache) {
      cache.putIfAbsent(getId(), mol);
    }

    return mol;
  }
}
