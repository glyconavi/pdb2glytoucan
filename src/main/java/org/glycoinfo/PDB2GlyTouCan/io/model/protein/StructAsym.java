package org.glycoinfo.PDB2GlyTouCan.io.model.protein;

public class StructAsym {

  private String id;
  private String pdbx_blank_PDB_chainid_flag;
  private String pdbx_modified;
  private String entity_id;
  private String details;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPdbx_blank_PDB_chainid_flag() {
    return pdbx_blank_PDB_chainid_flag;
  }

  public void setPdbx_blank_PDB_chainid_flag(String pdbx_blank_PDB_chainid_flag) {
    this.pdbx_blank_PDB_chainid_flag = pdbx_blank_PDB_chainid_flag;
  }

  public String getPdbx_modified() {
    return pdbx_modified;
  }

  public void setPdbx_modified(String pdbx_modified) {
    this.pdbx_modified = pdbx_modified;
  }

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }
}
