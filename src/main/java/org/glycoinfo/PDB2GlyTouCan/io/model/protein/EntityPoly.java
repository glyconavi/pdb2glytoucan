package org.glycoinfo.PDB2GlyTouCan.io.model.protein;

public class EntityPoly {

  private String entity_id;
  private String type;
  private String nstd_chirality;
  private String nstd_linkage;
  private String nstd_monomer;
  private String type_details;
  private String pdbx_seq_one_letter_code;
  private String pdbx_seq_one_letter_code_can;
  private String pdbx_strand_id;
  private String pdbx_target_identifier;

  public String getEntity_id() {
    return entity_id;
  }

  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getNstd_chirality() {
    return nstd_chirality;
  }

  public void setNstd_chirality(String nstd_chirality) {
    this.nstd_chirality = nstd_chirality;
  }

  public String getNstd_linkage() {
    return nstd_linkage;
  }

  public void setNstd_linkage(String nstd_linkage) {
    this.nstd_linkage = nstd_linkage;
  }

  public String getNstd_monomer() {
    return nstd_monomer;
  }

  public void setNstd_monomer(String nstd_monomer) {
    this.nstd_monomer = nstd_monomer;
  }

  public String getType_details() {
    return type_details;
  }

  public void setType_details(String type_details) {
    this.type_details = type_details;
  }

  public String getPdbx_seq_one_letter_code() {
    return pdbx_seq_one_letter_code;
  }

  public void setPdbx_seq_one_letter_code(String pdbx_seq_one_letter_code) {
    this.pdbx_seq_one_letter_code = pdbx_seq_one_letter_code;
  }

  public String getPdbx_seq_one_letter_code_can() {
    return pdbx_seq_one_letter_code_can;
  }

  public void setPdbx_seq_one_letter_code_can(String pdbx_seq_one_letter_code_can) {
    this.pdbx_seq_one_letter_code_can = pdbx_seq_one_letter_code_can;
  }

  public String getPdbx_strand_id() {
    return pdbx_strand_id;
  }

  public void setPdbx_strand_id(String pdbx_strand_id) {
    this.pdbx_strand_id = pdbx_strand_id;
  }

  public String getPdbx_target_identifier() {
    return pdbx_target_identifier;
  }

  public void setPdbx_target_identifier(String pdbx_target_identifier) {
    this.pdbx_target_identifier = pdbx_target_identifier;
  }
}
