package org.glycoinfo.PDB2GlyTouCan.io.model.protein;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PdbxStructModResidue {

  private String id;
  private String label_asym_id;
  private String label_comp_id;
  private String label_seq_id;
  private String parent_comp_id;
  private String details;
  private String PDB_ins_code;
  private String PDB_model_num;
  private String auth_asym_id;
  private String auth_comp_id;
  private String auth_seq_id;

  @JsonProperty("id")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @JsonProperty("label_asym_id")
  public String getLabel_asym_id() {
    return label_asym_id;
  }

  public void setLabel_asym_id(String label_asym_id) {
    this.label_asym_id = label_asym_id;
  }

  @JsonProperty("label_comp_id")
  public String getLabel_comp_id() {
    return label_comp_id;
  }

  public void setLabel_comp_id(String label_comp_id) {
    this.label_comp_id = label_comp_id;
  }

  @JsonProperty("label_seq_id")
  public String getLabel_seq_id() {
    return label_seq_id;
  }

  public void setLabel_seq_id(String label_seq_id) {
    this.label_seq_id = label_seq_id;
  }

  @JsonProperty("parent_comp_id")
  public String getParent_comp_id() {
    return parent_comp_id;
  }

  public void setParent_comp_id(String parent_comp_id) {
    this.parent_comp_id = parent_comp_id;
  }

  @JsonProperty("details")
  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  @JsonIgnore
  public String getPDB_ins_code() {
    return PDB_ins_code;
  }

  public void setPDB_ins_code(String PDB_ins_code) {
    this.PDB_ins_code = PDB_ins_code;
  }

  @JsonIgnore
  public String getPDB_model_num() {
    return PDB_model_num;
  }

  public void setPDB_model_num(String PDB_model_num) {
    this.PDB_model_num = PDB_model_num;
  }

  @JsonIgnore
  public String getAuth_asym_id() {
    return auth_asym_id;
  }

  public void setAuth_asym_id(String auth_asym_id) {
    this.auth_asym_id = auth_asym_id;
  }

  @JsonIgnore
  public String getAuth_comp_id() {
    return auth_comp_id;
  }

  public void setAuth_comp_id(String auth_comp_id) {
    this.auth_comp_id = auth_comp_id;
  }

  @JsonIgnore
  public String getAuth_seq_id() {
    return auth_seq_id;
  }

  public void setAuth_seq_id(String auth_seq_id) {
    this.auth_seq_id = auth_seq_id;
  }
}
