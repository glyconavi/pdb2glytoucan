package org.glycoinfo.PDB2GlyTouCan.io.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.structure.Proteindb;
import org.glycoinfo.PDB2GlyTouCan.structure.glyco.GlycoEntry;

import java.util.ArrayList;
import java.util.List;

public class GlyTouCan {

  @JsonProperty("PDB_id")
  private String PDB_id = "";

  @JsonProperty("time")
  private String time = "";

  @JsonProperty("date")
  private String date = "";

  @JsonProperty("Atom_site_label_asym_id")
  private List<String> Atom_site_label_asym_id = new ArrayList<>();

  @JsonProperty("protein_db")
  private List<Proteindb> protein_db = new ArrayList<>();

  @JsonProperty("ar_pdbx_struct_mod_residue")
  private List<PdbxStructModResidue> ar_pdbx_struct_mod_residue = new ArrayList<>();

  @JsonProperty("glyco_entries")
  public List<GlycoEntry> glyco_entries = new ArrayList<>();

  @JsonIgnore
  public String getPDB_id() {
    return PDB_id;
  }

  public void setPDB_id(String PDB_id) {
    this.PDB_id = PDB_id;
  }

  @JsonIgnore
  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  @JsonIgnore
  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  @JsonIgnore
  public List<String> getAtom_site_label_asym_id() {
    return Atom_site_label_asym_id;
  }

  public void setAtom_site_label_asym_id(List<String> atom_site_label_asym_id) {
    Atom_site_label_asym_id = atom_site_label_asym_id;
  }

  @JsonIgnore
  public List<Proteindb> getProtein_db() {
    return protein_db;
  }

  public void setProtein_db(List<Proteindb> protein_db) {
    this.protein_db = protein_db;
  }

  @JsonIgnore
  public List<PdbxStructModResidue> getAr_pdbx_struct_mod_residue() {
    return ar_pdbx_struct_mod_residue;
  }

  public void setAr_pdbx_struct_mod_residue(List<PdbxStructModResidue> ar_pdbx_struct_mod_residue) {
    this.ar_pdbx_struct_mod_residue = ar_pdbx_struct_mod_residue;
  }

  @JsonIgnore
  public List<GlycoEntry> getGlyco_entries() {
    return glyco_entries;
  }

  public void setGlyco_entries(List<GlycoEntry> glyco_entries) {
    this.glyco_entries = glyco_entries;
  }
}
