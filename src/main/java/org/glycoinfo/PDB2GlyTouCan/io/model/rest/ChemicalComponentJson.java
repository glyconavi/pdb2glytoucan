package org.glycoinfo.PDB2GlyTouCan.io.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChemicalComponentJson {

  private List<String> fields;
  private List<List<String>> results;

  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public List<List<String>> getResults() {
    return results;
  }

  public void setResults(List<List<String>> results) {
    this.results = results;
  }
}
