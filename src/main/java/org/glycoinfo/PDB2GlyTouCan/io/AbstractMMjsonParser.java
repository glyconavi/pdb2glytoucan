package org.glycoinfo.PDB2GlyTouCan.io;

import com.google.common.collect.Iterators;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public abstract class AbstractMMjsonParser {

  private Reader in;
  private JsonNode data;

  public AbstractMMjsonParser(Reader in) {
    this.in = in;
  }

  public AbstractMMjsonParser(String content) {
    this.in = new StringReader(content);
  }

  protected JsonNode getData() throws IOException, MMjsonParseException {
    if (data != null) {
      return data;
    }

    JsonNode json = new ObjectMapper().readTree(in);

    validateJson(json);

    String dataKey = Iterators.get(json.fields(), 0).getKey();

    data = json.get(dataKey);

    return data;
  }

  private void validateJson(JsonNode json) throws MMjsonParseException {
    if (json == null) {
      throw new MMjsonParseException("cannot parse null");
    }

    if (json.getNodeType() != JsonNodeType.OBJECT) {
      throw new MMjsonParseException(
          "expect OBJECT for root element but got " + json.getNodeType());
    }

    int rootEntrySize = Iterators.size(json.fieldNames());
    if (rootEntrySize != 1) {
      throw new MMjsonParseException(
          "expect only one entry on root object but got " + rootEntrySize);
    }
  }
}
