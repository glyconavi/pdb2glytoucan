package org.glycoinfo.PDB2GlyTouCan.io.file;

import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.structure.Mol;

import java.io.IOException;
import java.util.List;

public interface ChemicalComponentLoader {

  Mol load(String code) throws IOException, MMjsonParseException;

  List<String> saccharideIds();

}
