package org.glycoinfo.PDB2GlyTouCan.io.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Files {

  public static BufferedReader newBufferedReader(File file) throws IOException {
    return newBufferedReader(file, StandardCharsets.UTF_8);
  }

  public static BufferedReader newBufferedReader(File file, Charset cs) throws IOException {
    return new BufferedReader(new InputStreamReader(newInputStream(file), cs));
  }

  public static BufferedReader newBufferedReader(URL url) throws IOException {
    return newBufferedReader(url, StandardCharsets.UTF_8);
  }

  public static BufferedReader newBufferedReader(URL url, Charset cs) throws IOException {
    return new BufferedReader(new InputStreamReader(newInputStream(url), cs));
  }

  public static BufferedWriter newBufferedWriter(File file) throws IOException {
    return newBufferedWriter(file, StandardCharsets.UTF_8);
  }

  public static BufferedWriter newBufferedWriter(File file, Charset cs) throws IOException {
    return new BufferedWriter(new OutputStreamWriter(newOutputStream(file), cs));
  }

  public static OutputStream newGZIPOutputStream(OutputStream out) throws IOException {
    return new GZIPOutputStream(out);
  }

  private static InputStream newInputStream(File file) throws IOException {
    FileInputStream fis = new FileInputStream(file);

    if (file.getName().endsWith(".gz")) {
      return newGZIPInputStream(fis);
    } else {
      return fis;
    }
  }

  private static OutputStream newOutputStream(File file) throws IOException {
    FileOutputStream fos = new FileOutputStream(file);

    if (file.getName().endsWith(".gz")) {
      return newGZIPOutputStream(fos);
    } else {
      return fos;
    }
  }

  private static InputStream newInputStream(URL url) throws IOException {
    if (url.getFile().endsWith(".gz")) {
      return newGZIPInputStream(url.openStream());
    } else {
      return url.openStream();
    }
  }

  private static InputStream newGZIPInputStream(InputStream in) throws IOException {
    return new GZIPInputStream(in);
  }
}
