package org.glycoinfo.PDB2GlyTouCan.io.file;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.atteo.evo.inflector.English;
import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.ChemicalComponentJsonParser;
import org.glycoinfo.PDB2GlyTouCan.structure.Mol;
import org.glycoinfo.PDB2GlyTouCan.util.HTTP;
import org.glycoinfo.PDB2GlyTouCan.util.URLBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RemoteChemicalComponentLoader implements ChemicalComponentLoader {

  private static final Logger logger = LoggerFactory.getLogger(LocalChemicalComponentLoader.class);

  private static final Map<String, Mol> cache = new HashMap<>();

  private static List<String> saccharideIds;

  private static URL DEFAULT_BASE_URL;

  static {
    try {
      DEFAULT_BASE_URL = new URL("ftp://ftp.pdbj.org/mine2/data/cc/mmjson/");
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private URL baseUrl;

  public RemoteChemicalComponentLoader() {
    this(DEFAULT_BASE_URL);
  }

  public RemoteChemicalComponentLoader(String baseUrl) throws MalformedURLException {
    this(new URL(baseUrl));
  }

  public RemoteChemicalComponentLoader(URL baseUrl) {
    this.baseUrl = baseUrl;
  }

  @Override
  public Mol load(String code) throws IOException, MMjsonParseException {
    code = code.toUpperCase();

    if (cache.containsKey(code)) {
      return cache.get(code);
    }

    String fileName = String.format("%s.json.gz", code);

    try (BufferedReader br = Files.newBufferedReader(new URL(DEFAULT_BASE_URL, fileName))) {
      Mol mol = new ChemicalComponentJsonParser(br).parse().toMol();

      synchronized (cache) {
        cache.putIfAbsent(code, mol);
      }

      return cache.get(code);
    }
  }

  @Override
  public List<String> saccharideIds() {
    if (saccharideIds != null) {
      return saccharideIds;
    }

    logger.debug("Loading list of saccharide ID from remote");

    try {
      String url = URLBuilder.chemicalComponentFetchURL();

      logger.debug("GET " + url);

      JsonNode json = new ObjectMapper().readTree(HTTP.httpGet(url));
      Iterator<JsonNode> itrResults = json.get("results").iterator();

      List<String> result = new ArrayList<>();

      while (itrResults.hasNext()) {
        for (JsonNode jsonNode : itrResults.next()) {
          result.add(jsonNode.asText());
        }
      }

      logger.debug(
          String.format("Obtain %d %s", result.size(), English.plural("entry", result.size())));

      saccharideIds = result;
    } catch (IOException e) {
      logger.warn("Failed to obtain remote resource, use local data");

      saccharideIds = LocalChemicalComponentLoader.saccharideIds;
    }

    return saccharideIds;
  }
}
