package org.glycoinfo.PDB2GlyTouCan.io.file;

import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.ChemicalComponentJsonParser;
import org.glycoinfo.PDB2GlyTouCan.structure.Mol;
import org.glycoinfo.PDB2GlyTouCan.structure.SaccharideID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LocalChemicalComponentLoader implements ChemicalComponentLoader {

  private static final Logger logger = LoggerFactory.getLogger(LocalChemicalComponentLoader.class);

  private static final Map<String, Mol> cache = new HashMap<>();

  public static final List<String> saccharideIds = Arrays.stream(SaccharideID.values())
      .map(SaccharideID::toString)
      .collect(Collectors.toList());

  private String basePath;

  public LocalChemicalComponentLoader(String basePath) {
    this.basePath = basePath;
  }

  @Override
  public Mol load(String code) throws IOException, MMjsonParseException {
    code = code.toUpperCase();

    if (cache.containsKey(code)) {
      return cache.get(code);
    }

    String fileName = String.format("%s.json.gz", code);

    try (BufferedReader br = Files.newBufferedReader(Paths.get(basePath, fileName).toFile())) {
      Mol mol = new ChemicalComponentJsonParser(br).parse().toMol();

      synchronized (cache) {
        cache.putIfAbsent(code, mol);
      }

      return cache.get(code);
    }
  }

  @Override
  public List<String> saccharideIds() {
    return saccharideIds;
  }
}
