package org.glycoinfo.PDB2GlyTouCan.io;

import com.fasterxml.jackson.databind.JsonNode;

import org.atteo.evo.inflector.English;
import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.model.Protein;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.ChemComp;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.Entry;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructAsym;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructConn;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructRefSeq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PdbJsonParser extends AbstractMMjsonParser {

  private static final Logger logger = LoggerFactory.getLogger(PdbJsonParser.class);

  public PdbJsonParser(Reader in) {
    super(in);
  }

  public PdbJsonParser(String content) {
    super(content);
  }

  public Protein parse() throws IOException, MMjsonParseException {
    JsonNode data = getData();

    String id = getData().get("entry").get("id").get(0).asText();
    logger.debug("Parsing pdb entry: " + id);

    Protein protein = new Protein();

    protein.setEntry(parseEntry(data.get("entry")));

    protein.setAtomSites(parseAtomSite(data.get("atom_site")));
    protein.setChemComps(parseChemComp(data.get("chem_comp")));
    protein.setEntityPolies(parseEntityPoly(data.get("entity_poly")));
    protein.setPdbxStructModResidues(parsePdbxStructModResidue(data.get("pdbx_struct_mod_residue")));
    protein.setStructAsyms(parseStructAsym(data.get("struct_asym")));
    protein.setStructRefSeqs(parseStructRefSeq(data.get("struct_ref_seq")));
    protein.setStructConns(parseStructConn(data.get("struct_conn")));

    return protein;
  }

  private List<Entry> parseEntry(JsonNode node) {
    List<Entry> entries = new ArrayList<>();

    Iterator<JsonNode> itrId = node.get("id").iterator();
    while (itrId.hasNext()) {
      Entry entry = new Entry();

      entry.setId(itrId.next().asText());

      entries.add(entry);
    }

    logger.debug(
        String.format("Parse %d %s", entries.size(), English.plural("entry", entries.size())));

    return entries;
  }

  private List<AtomSite> parseAtomSite(JsonNode node) throws MMjsonParseException {
    if (node == null) {
      logger.debug("No atom_site found");
      return Collections.emptyList();
    }

    List<AtomSite> atomSites = new ArrayList<>();

    Iterator<JsonNode> itrId = node.get("id").iterator();
    Iterator<JsonNode> itrAuth_asym_id = node.get("auth_asym_id").iterator();
    Iterator<JsonNode> itrGroup_PDB = node.get("group_PDB").iterator();
    Iterator<JsonNode> itrLabel_atom_id = node.get("label_atom_id").iterator();
    Iterator<JsonNode> itrLabel_comp_id = node.get("label_comp_id").iterator();
    Iterator<JsonNode> itrLabel_seq_id = node.get("label_seq_id").iterator();
    Iterator<JsonNode> itrLabel_entity_id = node.get("label_entity_id").iterator();
    Iterator<JsonNode> itrLabel_asym_id = node.get("label_asym_id").iterator();
    Iterator<JsonNode> itrPdbx_PDB_model_num = node.get("pdbx_PDB_model_num").iterator();
    Iterator<JsonNode> itrType_symbol = node.get("type_symbol").iterator();
    Iterator<JsonNode> itrCartn_x = node.get("Cartn_x").iterator();
    Iterator<JsonNode> itrCartn_y = node.get("Cartn_y").iterator();
    Iterator<JsonNode> itrCartn_z = node.get("Cartn_z").iterator();
    Iterator<JsonNode> itrOccupancy = node.get("occupancy").iterator();
    Iterator<JsonNode> itrPdbx_PDB_ins_code = node.get("pdbx_PDB_ins_code").iterator();

    while (itrId.hasNext()) {
      AtomSite atomSite = new AtomSite();

      String modelNum = itrPdbx_PDB_model_num.next().asText();
      if (modelNum.length() > 7) {
        throw new MMjsonParseException(
            String
                .format("%s is too long for model number, expect 7 characters or less", modelNum));
      }

      atomSite.setId(itrId.next().asText());
      atomSite.setAuth_asym_id(itrAuth_asym_id.next().asText());
      atomSite.setGroup_PDB(itrGroup_PDB.next().asText());
      atomSite.setLabel_atom_id(itrLabel_atom_id.next().asText());
      atomSite.setLabel_comp_id(itrLabel_comp_id.next().asText());
      atomSite.setLabel_seq_id(itrLabel_seq_id.next().asText());
      atomSite.setLabel_entity_id(itrLabel_entity_id.next().asText());
      atomSite.setLabel_asym_id(itrLabel_asym_id.next().asText());
      atomSite.setPdbx_PDB_model_num(modelNum);
      atomSite.setType_symbol(itrType_symbol.next().asText());

      atomSite.setCartn_x(itrCartn_x.next().asText());
      atomSite.setCartn_y(itrCartn_y.next().asText());
      atomSite.setCartn_z(itrCartn_z.next().asText());
      atomSite.setOccupancy(itrOccupancy.next().asText());
      atomSite.setPdbx_PDB_ins_code(itrPdbx_PDB_ins_code.next().asText());

      atomSites.add(atomSite);
    }

    logger.debug(String
        .format("Parse %d %s", atomSites.size(), English.plural("atom_site", atomSites.size())));

    return atomSites;
  }

  private List<ChemComp> parseChemComp(JsonNode node) {
    if (node == null) {
      logger.debug("No chem_comp found");
      return Collections.emptyList();
    }

    List<ChemComp> chemComps = new ArrayList<>();

    Iterator<JsonNode> itrId = node.get("id").iterator();
    Iterator<JsonNode> itrName = node.get("name").iterator();

    while (itrId.hasNext()) {
      ChemComp chemComp = new ChemComp();

      chemComp.setId(itrId.next().asText());
      chemComp.setName(itrName.next().asText());

      chemComps.add(chemComp);
    }

    logger.debug(String
        .format("Parse %d %s", chemComps.size(), English.plural("chem_comp", chemComps.size())));

    return chemComps;
  }

  private List<EntityPoly> parseEntityPoly(JsonNode node) {
    if (node == null) {
      logger.debug("No entity_poly found");
      return Collections.emptyList();
    }

    List<EntityPoly> entityPolies = new ArrayList<>();

    Iterator<JsonNode> itrEntityId = node.get("entity_id").iterator();
    Iterator<JsonNode> itrType = node.get("type").iterator();
    Iterator<JsonNode> itrPdbx_seq_one_letter_code_can = node.get("pdbx_seq_one_letter_code_can")
        .iterator();
    Iterator<JsonNode> itrPdbx_strand_id = node.get("pdbx_strand_id").iterator();

    while (itrEntityId.hasNext()) {
      EntityPoly entityPoly = new EntityPoly();

      entityPoly.setEntity_id(itrEntityId.next().asText());
      entityPoly.setType(itrType.next().asText());
      entityPoly.setPdbx_seq_one_letter_code_can(itrPdbx_seq_one_letter_code_can.next().asText());
      entityPoly.setPdbx_strand_id(itrPdbx_strand_id.next().asText());

      entityPolies.add(entityPoly);
    }

    logger.debug(String.format("Parse %d %s", entityPolies.size(),
        English.plural("entity_poly", entityPolies.size())));

    return entityPolies;
  }

  private List<PdbxStructModResidue> parsePdbxStructModResidue(JsonNode node) {
    if (node == null) {
      logger.debug("No pdbx_struct_mod_residue found");
      return Collections.emptyList();
    }

    List<PdbxStructModResidue> pdbxStructModResidues = new ArrayList<>();

    Iterator<JsonNode> itrId = node.get("id").iterator();
    Iterator<JsonNode> itrLabel_asym_id = node.get("label_asym_id").iterator();
    Iterator<JsonNode> itrLabel_comp_id = node.get("label_comp_id").iterator();
    Iterator<JsonNode> itrLabel_seq_id = node.get("label_seq_id").iterator();
    Iterator<JsonNode> itrParent_comp_id = node.get("parent_comp_id").iterator();
    Iterator<JsonNode> itrDetails = node.get("details").iterator();

    while (itrId.hasNext()) {
      PdbxStructModResidue pdbxStructModResidue = new PdbxStructModResidue();

      pdbxStructModResidue.setId(itrId.next().asText());
      pdbxStructModResidue.setLabel_asym_id(itrLabel_asym_id.next().asText());
      pdbxStructModResidue.setLabel_comp_id(itrLabel_comp_id.next().asText());
      pdbxStructModResidue.setLabel_seq_id(itrLabel_seq_id.next().asText());
      pdbxStructModResidue.setParent_comp_id(itrParent_comp_id.next().asText());
      pdbxStructModResidue.setDetails(itrDetails.next().asText());

      pdbxStructModResidues.add(pdbxStructModResidue);
    }

    logger.debug(String.format("Parse %d %s", pdbxStructModResidues.size(),
        English.plural("pdbx_struct_mod_residue", pdbxStructModResidues.size())));

    return pdbxStructModResidues;
  }

  private List<StructAsym> parseStructAsym(JsonNode node) {
    if (node == null) {
      logger.debug("No struct_asym found");
      return Collections.emptyList();
    }

    List<StructAsym> structAsyms = new ArrayList<>();

    Iterator<JsonNode> itrId = node.get("id").iterator();
    Iterator<JsonNode> itrEntityId = node.get("entity_id").iterator();

    while (itrId.hasNext()) {
      StructAsym structAsym = new StructAsym();

      structAsym.setId(itrId.next().asText());
      structAsym.setEntity_id(itrEntityId.next().asText());

      structAsyms.add(structAsym);
    }

    logger.debug(String.format("Parse %d %s", structAsyms.size(),
        English.plural("struct_asym", structAsyms.size())));

    return structAsyms;
  }

  private List<StructConn> parseStructConn(JsonNode node) {
    if (node == null) {
      logger.debug("No struct_conn found");
      return Collections.emptyList();
    }

    List<StructConn> structConns = new ArrayList<>();

    Iterator<JsonNode> itrId = node.get("id").iterator();
    Iterator<JsonNode> itrConn_type_id = node.get("conn_type_id").iterator();
    Iterator<JsonNode> itrPdbx_dist_value = node.get("pdbx_dist_value").iterator();
    Iterator<JsonNode> itrPdbx_PDB_id = node.get("pdbx_PDB_id").iterator();
    Iterator<JsonNode> itrPtnr1_label_asym_id = node.get("ptnr1_label_asym_id").iterator();
    Iterator<JsonNode> itrPtnr1_label_comp_id = node.get("ptnr1_label_comp_id").iterator();
    Iterator<JsonNode> itrPtnr1_label_seq_id = node.get("ptnr1_label_seq_id").iterator();
    Iterator<JsonNode> itrPtnr1_label_atom_id = node.get("ptnr1_label_atom_id").iterator();
    Iterator<JsonNode> itrPtnr2_label_asym_id = node.get("ptnr2_label_asym_id").iterator();
    Iterator<JsonNode> itrPtnr2_label_comp_id = node.get("ptnr2_label_comp_id").iterator();
    Iterator<JsonNode> itrPtnr2_label_seq_id = node.get("ptnr2_label_seq_id").iterator();
    Iterator<JsonNode> itrPtnr2_label_atom_id = node.get("ptnr2_label_atom_id").iterator();

    while (itrId.hasNext()) {
      StructConn structConn = new StructConn();

      structConn.setId(itrId.next().asText());
      structConn.setConn_type_id(itrConn_type_id.next().asText());
      structConn.setPdbx_dist_value(itrPdbx_dist_value.next().asText());
      structConn.setPdbx_PDB_id(itrPdbx_PDB_id.next().asText());
      structConn.setPtnr1_label_asym_id(itrPtnr1_label_asym_id.next().asText());
      structConn.setPtnr1_label_comp_id(itrPtnr1_label_comp_id.next().asText());
      structConn.setPtnr1_label_seq_id(itrPtnr1_label_seq_id.next().asText());
      structConn.setPtnr1_label_atom_id(itrPtnr1_label_atom_id.next().asText());
      structConn.setPtnr2_label_asym_id(itrPtnr2_label_asym_id.next().asText());
      structConn.setPtnr2_label_comp_id(itrPtnr2_label_comp_id.next().asText());
      structConn.setPtnr2_label_seq_id(itrPtnr2_label_seq_id.next().asText());
      structConn.setPtnr2_label_atom_id(itrPtnr2_label_atom_id.next().asText());

      structConns.add(structConn);
    }

    logger.debug(String.format("Parse %d %s", structConns.size(),
        English.plural("struct_conn", structConns.size())));

    return structConns;
  }

  private List<StructRefSeq> parseStructRefSeq(JsonNode node) {
    if (node == null) {
      logger.debug("No struct_ref_seq found");
      return Collections.emptyList();
    }

    List<StructRefSeq> structRefSeqs = new ArrayList<>();

    Iterator<JsonNode> itrAlign_id = node.get("align_id").iterator();
    Iterator<JsonNode> itrPdbx_strand_id = node.get("pdbx_strand_id").iterator();
    Iterator<JsonNode> itrPdbx_db_accession = node.get("pdbx_db_accession").iterator();

    while (itrAlign_id.hasNext()) {
      StructRefSeq structRefSeq = new StructRefSeq();

      structRefSeq.setAlign_id(itrAlign_id.next().asText());
      structRefSeq.setPdbx_strand_id(itrPdbx_strand_id.next().asText());
      structRefSeq.setPdbx_db_accession(itrPdbx_db_accession.next().asText());

      structRefSeqs.add(structRefSeq);
    }

    logger.debug(String.format("Parse %d %s", structRefSeqs.size(),
        English.plural("struct_ref_seq", structRefSeqs.size())));

    return structRefSeqs;
  }
}
