package org.glycoinfo.PDB2GlyTouCan.io;

import com.fasterxml.jackson.databind.JsonNode;

import org.atteo.evo.inflector.English;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompAtom;
import org.biojava.nbio.structure.io.mmcif.model.ChemCompBond;
import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.model.ChemicalComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ChemicalComponentJsonParser extends AbstractMMjsonParser {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponentJsonParser.class);

  public ChemicalComponentJsonParser(Reader in) {
    super(in);
  }

  public ChemicalComponentJsonParser(String content) {
    super(content);
  }

  public ChemicalComponent parse() throws IOException, MMjsonParseException {
    JsonNode data = getData();

    String id = getData().get("chem_comp").get("id").get(0).asText();
    logger.debug("Parsing chemical component: " + id);

    ChemicalComponent cc = new ChemicalComponent();

    cc.setId(id);
    cc.setChemCompAtoms(parseChemCompAtom(data.get("chem_comp_atom")));
    cc.setChemCompBonds(parseChemCompBond(data.get("chem_comp_bond")));

    return cc;
  }

  private List<ChemCompAtom> parseChemCompAtom(JsonNode node) {
    List<ChemCompAtom> chemCompAtoms = new ArrayList<>();

    Iterator<JsonNode> itrAtom_id = node.get("atom_id").iterator();
    Iterator<JsonNode> itrType_symbol = node.get("type_symbol").iterator();
    Iterator<JsonNode> itrCharge = node.get("charge").iterator();
    Iterator<JsonNode> itrPdbx_model_Cartn_x_ideal = node.get("pdbx_model_Cartn_x_ideal")
        .iterator();
    Iterator<JsonNode> itrPdbx_model_Cartn_y_ideal = node.get("pdbx_model_Cartn_y_ideal")
        .iterator();
    Iterator<JsonNode> itrPdbx_model_Cartn_z_ideal = node.get("pdbx_model_Cartn_z_ideal")
        .iterator();

    while (itrAtom_id.hasNext()) {
      ChemCompAtom chemCompAtom = new ChemCompAtom();

      chemCompAtom.setAtom_id(itrAtom_id.next().asText());
      chemCompAtom.setType_symbol(itrType_symbol.next().asText());
      chemCompAtom.setCharge(itrCharge.next().asText());
      chemCompAtom.setPdbx_model_Cartn_x_ideal(itrPdbx_model_Cartn_x_ideal.next().asText());
      chemCompAtom.setPdbx_model_Cartn_y_ideal(itrPdbx_model_Cartn_y_ideal.next().asText());
      chemCompAtom.setPdbx_model_Cartn_z_ideal(itrPdbx_model_Cartn_z_ideal.next().asText());

      chemCompAtoms.add(chemCompAtom);
    }

    logger.debug(String.format("Parse %d %s", chemCompAtoms.size(),
        English.plural("chem_comp_atom", chemCompAtoms.size())));

    return chemCompAtoms;
  }

  private List<ChemCompBond> parseChemCompBond(JsonNode node) {
    List<ChemCompBond> chemCompBonds = new ArrayList<>();

    Iterator<JsonNode> itrAtom_id_1 = node.get("atom_id_1").iterator();
    Iterator<JsonNode> itrAtom_id_2 = node.get("atom_id_2").iterator();
    Iterator<JsonNode> itrValue_order = node.get("value_order").iterator();
    Iterator<JsonNode> itrPdbx_aromatic_flag = node.get("pdbx_aromatic_flag").iterator();

    while (itrAtom_id_1.hasNext()) {
      ChemCompBond chemCompBond = new ChemCompBond();

      chemCompBond.setAtom_id_1(itrAtom_id_1.next().asText());
      chemCompBond.setAtom_id_2(itrAtom_id_2.next().asText());
      chemCompBond.setValue_order(itrValue_order.next().asText());
      chemCompBond.setPdbx_aromatic_flag(itrPdbx_aromatic_flag.next().asText());

      chemCompBonds.add(chemCompBond);
    }

    logger.debug(String.format("Parse %d %s", chemCompBonds.size(),
        English.plural("chem_comp_bond", chemCompBonds.size())));

    return chemCompBonds;
  }

}
