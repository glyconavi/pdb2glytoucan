package org.glycoinfo.PDB2GlyTouCan.structure;

import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.file.ChemicalComponentLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ChemicalComponentDictionary {

  private static final Logger logger = LoggerFactory.getLogger(ChemicalComponentDictionary.class);

  private ChemicalComponentLoader ccLoader;

  public ChemicalComponentDictionary(ChemicalComponentLoader ccLoader) {
    this.ccLoader = ccLoader;
  }

  public List<Bond> bondsFor(List<Atom> atoms) throws IOException, MMjsonParseException {
    List<Bond> out_bonds = new LinkedList<>();

    Set<String> saccharides = new HashSet<>(ccLoader.saccharideIds());

    for (int i = 0; i < atoms.size(); i++) {
      logger.debug(String.format("Loop %d", i));

      if (saccharides.contains(atoms.get(i).getComp_id())) {

        Mol i_cc_mol = ccLoader.load(atoms.get(i).getComp_id());

        for (int j = 0; j < atoms.size(); j++) {
          if (i > j) {

            if (atoms.get(i).getAtom_site_label_asym_id()
                .equals(atoms.get(j).getAtom_site_label_asym_id())) {

              if (saccharides.contains(atoms.get(i).getComp_id())) {
                Mol j_cc_mol = ccLoader.load(atoms.get(j).getComp_id());

                String atom1 = atoms.get(i).getAtom_site_label_atom_id();
                String atom2 = atoms.get(j).getAtom_site_label_atom_id();

                int bond_order = i_cc_mol.getBondOrder(atom1, atom2);

                if (bond_order == 0) {
                  bond_order = j_cc_mol.getBondOrder(atoms.get(j).getAtom_site_label_atom_id(),
                      atoms.get(i).getAtom_site_label_atom_id());
                }

                if (bond_order > 0) {
                  Bond bd = new Bond(atoms.get(i), atoms.get(j));
                  bd.setBondOrder(bond_order);
                  out_bonds.add(bd);
                }
              }
            }
          }
        }
      } else {
        logger.warn("Not found in dictionary: " + atoms.get(i).getComp_id());
      }
    }
    return out_bonds;
  }
}
