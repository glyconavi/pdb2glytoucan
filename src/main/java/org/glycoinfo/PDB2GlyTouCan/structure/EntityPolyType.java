package org.glycoinfo.PDB2GlyTouCan.structure;

/**
 * The type of the polymer.
 */
public enum EntityPolyType {

  CYCLIC_PSEUDO_PEPTIDE("cyclic-pseudo-peptide"),
  OTHER("other"),
  PEPTIDE_NUCLEIC_ACID("peptide nucleic acid"),
  POLYDEOXYRIBONUCLEOTIDE("polydeoxyribonucleotide"),
  POLYDEOXYRIBONUCLEOTIDE_POLYRIBONUCLEOTIDE_HYBRID("polydeoxyribonucleotide/polyribonucleotide hybrid"),
  POLYPEPTIDE_D("polypeptide(D)"),
  POLYPEPTIDE_L("polypeptide(L)"),
  POLYRIBONUCLEOTIDE("polyribonucleotide"),
  POLYSACCHARIDE_D("polysaccharide(D)"),
  POLYSACCHARIDE_L("polysaccharide(L)");

  public static EntityPolyType lookUp(String label) {
    for (EntityPolyType ept : EntityPolyType.values()) {
      if (ept.toString().equals(label)) {
        return ept;
      }
    }

    return null;
  }

  private final String label;

  EntityPolyType(final String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return label;
  }

}
