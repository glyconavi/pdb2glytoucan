package org.glycoinfo.PDB2GlyTouCan.structure;

public class Bond {

  private int id;

  private Atom fromAtom;
  private Atom toAtom;

  private int BondOrder;
  private double length;
  private Boolean Aromoatic;

  public Bond() {
  }

  public Bond(Atom fromAtom, Atom toAtom) {
    this.fromAtom = fromAtom;
    this.toAtom = toAtom;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Atom getFromAtom() {
    return fromAtom;
  }

  public void setFromAtom(Atom fromAtom) {
    this.fromAtom = fromAtom;
  }

  public Atom getToAtom() {
    return toAtom;
  }

  public void setToAtom(Atom toAtom) {
    this.toAtom = toAtom;
  }

  public int getBondOrder() {
    return BondOrder;
  }

  public void setBondOrder(int bondOrder) {
    BondOrder = bondOrder;
  }

  public double getLength() {
    return length();
  }

  @Deprecated
  public void setLength(double length) {
    this.length = length;
  }

  public Boolean getAromoatic() {
    return Aromoatic;
  }

  public void setAromoatic(Boolean aromoatic) {
    Aromoatic = aromoatic;
  }

  private double length() {
    if (fromAtom == null || toAtom == null) {
      return 0D;
    }

    return Math.sqrt(Math.pow((toAtom.getX() - fromAtom.getX()), 2D)
        + Math.pow((toAtom.getY() - fromAtom.getY()), 2D)
        + Math.pow((toAtom.getZ() - fromAtom.getZ()), 2D));
  }
}
