package org.glycoinfo.PDB2GlyTouCan.structure;

import java.util.LinkedList;

public class Chem {

//  synchronized public static LinkedList<Bond> getBonds4CC(List<Atom> a_atoms_l,
//      String cc_local_path) {
//    LinkedList<Bond> out_bonds = new LinkedList<Bond>();
////    LinkedList<String> PDBCC_Saccharide_idArray = ChemicalComponentDictionary.getPDBCCidArray();
//
//    LinkedList<String> PDBCC_Saccharide_idArray = ChemicalComponentDictionary.getPDBCCidArray();
//
//    HashSet<String> PDBCC_Saccharide_idArray_hs = new HashSet<>(PDBCC_Saccharide_idArray);
//    ArrayList<Atom> a_atoms = new ArrayList<>(a_atoms_l);
//    for (int i = 0; i < a_atoms.size(); i++) {
//
//      if (PDBCC_Saccharide_idArray_hs.contains(a_atoms.get(i).getComp_id())) {
//
//        Mol i_cc_mol = ChemicalComponent.getPDBccMol(a_atoms.get(i).getComp_id(), cc_local_path);
//
//        for (int j = 0; j < a_atoms.size(); j++) {
//          if (i > j) {
//
//            if (a_atoms.get(i).getAtom_site_label_asym_id()
//                .equals(a_atoms.get(j).getAtom_site_label_asym_id())) {
//
//              if (PDBCC_Saccharide_idArray_hs.contains(a_atoms.get(i).getComp_id())) {
//                Mol j_cc_mol = ChemicalComponent
//                    .getPDBccMol(a_atoms.get(j).getComp_id(), cc_local_path);
//
//                int bond_order = i_cc_mol.getBondOrder(a_atoms.get(i).getAtom_site_label_atom_id() //.replaceAll("\"", "")
//                    , a_atoms.get(j).getAtom_site_label_atom_id() //.replaceAll("\"", "")
//                );
//
//                if (bond_order == 0) {
//                  bond_order = j_cc_mol.getBondOrder(a_atoms.get(j).getAtom_site_label_atom_id() //.replaceAll("\"", "")
//                      , a_atoms.get(i).getAtom_site_label_atom_id() //.replaceAll("\"", "")
//                  );
//                }
//
//                if (bond_order > 0) {
//                  double length = Calc.bond_length(a_atoms.get(i), a_atoms.get(j));
//                  //System.out.println("leng: "+ length);;
//                  Bond bd = new Bond();
//                  bd.setFromAtom(a_atoms.get(i));
//                  bd.setToAtom(a_atoms.get(j));
//                  bd.setBondOrder(bond_order);
//                  bd.setLength(length);
//                  out_bonds.add(bd);
//                }
//              }
//            }
//          }
//        }
//      } else {
//
//        System.err.println(a_atoms.get(i).getComp_id() + " was not found in dictionaly.");
//      }
//    }
//    return out_bonds;
//  }


  public static LinkedList<Bond> getBonds(LinkedList<Atom> a_atoms) {
    LinkedList<Bond> out_bonds = new LinkedList<Bond>();

    for (int i = 0; i < a_atoms.size(); i++) {
      for (int j = 0; j < a_atoms.size(); j++) {
        if (i < j) {
          int bond_order = Chem.bondtype4length(a_atoms.get(i), a_atoms.get(j));
          if (bond_order > 0) {
            Bond bd = new Bond();
            bd.setFromAtom(a_atoms.get(i));
            bd.setToAtom(a_atoms.get(j));
            bd.setBondOrder(bond_order);
            out_bonds.add(bd);
          }
        }
      }
    }
    return out_bonds;
  }


  /// <summary>
  /// Bondtype between a1 and a2. single:1, double:2, triple:3, aromatic:4
  /// </summary>
  /// <param name="a1">A1.</param>
  /// <param name="a2">A2.</param>
  public static int bondtype4length(Atom a1, Atom a2) {
    int bondorder = 0;

    //原子間距離
    double length = new Bond(a1, a2).getLength();

    // http://www.h6.dion.ne.jp/~k-sugino/modeling/covalent.html

    // https://ja.wikipedia.org/wiki/%E3%82%A2%E3%83%AB%E3%82%AD%E3%83%B3
    // C−C 結合距離は 121 pm であり、これはアルケンの 134 pm、アルカンの 153 pm と比べ短い。

    // type vdw+ PDB(3AIC)
    // C-C	1.50 1.45-1.65
    // C=C 1.34 1.3-1.45
    // C#C 1.21
    // C-N 1.46 1.4-1.7
    // C=N 1.27 1.25-1.4
    // C-O 1.38 1.3-1.55
    // C=O 1.24 1.15-1.3
    // N-N 1.42
    // N=N 1.20
    // N-O 1.34
    // N=O 1.17
    // O-O 1.26
    // O=O 1.14

    float vdw_pdb_factor = 1.5f;
    float min_vdw_pdb_factor = 0.3f;
    //各原子のVDWの和
    double vdwvdw =
        ((VDW(a1.getAtom_site_type_symbol()) + VDW(a2.getAtom_site_type_symbol())) * 0.5f)
            * vdw_pdb_factor;
    double min_vdwvdw =
        ((VDW(a1.getAtom_site_type_symbol()) + VDW(a2.getAtom_site_type_symbol())) * 0.5f)
            * min_vdw_pdb_factor;

    //System.out.println("vdwvdw: "+ vdwvdw);
    //比較
    if (length < vdwvdw && length > min_vdwvdw) {
      //System.out.println(a1.getAtom_site_type_symbol() + "-"+ a2.getAtom_site_type_symbol() + " length: " + String.format ("%10.4f",length));

      // CO bond
      if ((a1.getAtom_site_type_symbol().equals("C") && a2.getAtom_site_type_symbol().equals("O"))
          ||
          (a1.getAtom_site_type_symbol().equals("O") && a2.getAtom_site_type_symbol()
              .equals("C"))) {
        if (length > 1.0) {
					/*
					if (length < 1.28 && length > 1.1) {
						bondorder = 2;
					} else if (length >= 1.28 && length < 1.8) {
						bondorder = 1;
					}
					*/
          bondorder = 1;
        }
      }
      // CN bond
      if ((a1.getAtom_site_type_symbol().equals("C") && a2.getAtom_site_type_symbol().equals("N"))
          ||
          (a1.getAtom_site_type_symbol().equals("N") && a2.getAtom_site_type_symbol()
              .equals("C"))) {
        if (length > 1.0) {
					/*
					if (length < 1.35 && length >= 1.2) {
						bondorder = 2;
					} else if (length < 1.2 && length > 1.0) {
						bondorder = 3;
					} else if (length >= 1.35 && length < 1.8) {
						bondorder = 1;
					}
					*/
          bondorder = 1;
        }
      }
      // CC bond
      if (a1.getAtom_site_type_symbol().equals("C") && a2.getAtom_site_type_symbol().equals("C")) {
        if (length > 1.0) {
					/*
					if (length < 1.25 && length > 1.20) {
						bondorder = 3;
					} else if (length <= 1.25 && length < 1.4) {
						bondorder = 2;
					} else if (length >= 1.4 && length < 1.8) {
						bondorder = 1;
					}
					*/
          bondorder = 1;
        }
      }
      // NN bond
      if (a1.getAtom_site_type_symbol().equals("N") && a2.getAtom_site_type_symbol().equals("N")) {
        if (length > 1.0) {
          if (length < 1.3) {
            bondorder = 2;
          } else if (length >= 1.3 && length < 1.5) {
            bondorder = 1;
          }
        }
      }
      // NO bond
      if ((a1.getAtom_site_type_symbol().equals("N") && a2.getAtom_site_type_symbol().equals("O"))
          ||
          (a1.getAtom_site_type_symbol().equals("O") && a2.getAtom_site_type_symbol()
              .equals("N"))) {
        if (length > 1.0) {
          if (length < 1.2) {
            bondorder = 2;
          } else if (length >= 1.2 && length < 1.4) {
            bondorder = 1;
          }
        }
      }
      // others
      if (bondorder == 0) {
        if (length > 1.2 && length < 1.7) {
          bondorder = 1;
        }
        if (length > 1.0) {
          bondorder = 1;
        }
      }
      //Console.WriteLine (a1.Symbol + "-" + a2.Symbol + ": " + bondorder + "; " + ln + ", " + vdwvdw);
    }

    //if (Math.abs(length) < vdwvdw) {
    //	bondorder =1;
    //}
    return bondorder;
  }

  public static double VDW(String AtomicSymbol) {
    //A Bondi (1964) J Phys Chem 68:441
    // A
    switch (AtomicSymbol) {
      case "H":
        return 1.20;
      case "He":
        return 1.40;
      case "Li":
        return 1.82;
      case "C":
        return 1.70;
      case "N":
        return 1.55;
      case "O":
        return 1.52;
      case "F":
        return 1.47;
      case "Ne":
        return 1.54;
      case "Na":
        return 2.27;
      case "Mg":
        return 1.73;
      case "Si":
        return 2.10;
      case "P":
        return 1.80;
      case "S":
        return 1.80;
      case "Cl":
        return 1.75;
      case "Ar":
        return 1.88;
      case "K":
        return 2.75;
      case "Ni":
        return 1.63;
      case "Cu":
        return 1.40;
      case "Zn":
        return 1.39;
      case "Ga":
        return 1.87;
      case "As":
        return 1.85;
      case "Se":
        return 1.90;
      case "Br":
        return 1.85;
      case "Kr":
        return 2.02;
      case "Pd":
        return 1.63;
      case "Ag":
        return 1.72;
      case "Cd":
        return 1.58;
      case "In":
        return 1.93;
      case "Sn":
        return 2.17;
      case "Te":
        return 2.06;
      case "I":
        return 1.98;
      case "Xe":
        return 2.16;
      case "Pt":
        return 1.75;
      case "Au":
        return 1.66;
      case "Hg":
        return 1.55;
      case "TI":
        return 1.96;
      case "Pb":
        return 2.02;
      case "U":
        return 1.86;
      default:
        return 2.00;

    }
  }


}
