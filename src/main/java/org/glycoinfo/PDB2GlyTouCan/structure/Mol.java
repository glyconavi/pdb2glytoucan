package org.glycoinfo.PDB2GlyTouCan.structure;

import java.util.List;

public class Mol {

  private List<Atom> atoms;
  private List<Bond> bonds;

  public Mol(List<Atom> a_atoms, List<Bond> a_bonds) {
    this.atoms = a_atoms;
    this.bonds = a_bonds;
  }

  public List<Atom> getAtoms() {
    return atoms;
  }

  public void setAtoms(List<Atom> atoms) {
    this.atoms = atoms;
  }

  public List<Bond> getBonds() {
    return bonds;
  }

  public void setBonds(List<Bond> bonds) {
    this.bonds = bonds;
  }

  public int getBondOrder(String a_atom_id_1, String a_atom_id_2) {
    for (int i = 0; i < getBonds().size(); i++) {
      if (getBonds().get(i).getFromAtom().getAtom_site_label_atom_id().equals(a_atom_id_1)) {
        if (getBonds().get(i).getToAtom().getAtom_site_label_atom_id().equals(a_atom_id_2)) {
          return getBonds().get(i).getBondOrder();
        }
      } else if (getBonds().get(i).getFromAtom().getAtom_site_label_atom_id()
          .equals(a_atom_id_2)) {
        if (getBonds().get(i).getToAtom().getAtom_site_label_atom_id().equals(a_atom_id_1)) {
          return getBonds().get(i).getBondOrder();
        }
      }
    }

    return 0;
  }
}
