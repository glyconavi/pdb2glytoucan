package org.glycoinfo.PDB2GlyTouCan.structure;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ConnectedEntry {

  public ArrayList<ConnectedEntryModel> models = new ArrayList<>();
  public ArrayList<PdbxStructModResidue> parentResidues = new ArrayList<>();

  public void addParentResidue(PdbxStructModResidue pp) {
    parentResidues.add(pp);
  }

  public void addModel(ConnectedEntryModel cm) {
    models.add(cm);
    cm.setParent(this);
  }

  public boolean sameWith(ConnectedEntryModel cm) {
    for (ConnectedEntryModel mm : models) {
      if (mm.sameWith(cm)) {
        return true;
      }
    }
    return false;
  }


  public static ArrayList<ModelCassette> entryModelToCassette(String pdbcode,
                                                              ArrayList<ConnectedEntry> models) {
    ArrayList<ModelCassette> ret = new ArrayList<>();
    HashMap<String, ArrayList<ConnectedEntryModel>> model_i = new HashMap<>();
    for (ConnectedEntry ce : models) {
      for (ConnectedEntryModel cmm : ce.models) {
        if (!model_i.containsKey(cmm.getModelNum())) {
          model_i.put(cmm.getModelNum(), new ArrayList<>());
        }
        model_i.get(cmm.getModelNum()).add(cmm);
      }
    }

    ArrayList<String> da = new ArrayList<>(model_i.keySet());
    Collections.sort(da);

    for (String modelid : da) {
      ArrayList<ConnectedEntryModel> ma = model_i.get(modelid);
      for (ConnectedEntryModel cm : ma) {
        ModelCassette mc = new ModelCassette();
        mc.pdbx_PDB_model_num = modelid;
        ConnectedEntry pare = cm.getParent();
        PdbxStructModResidue pres = null;
        if (pare.parentResidues.size() > 0) {
          pres = pare.parentResidues.get(0);
          if (pare.parentResidues.size() > 1) {
            System.err.println("??? this compound is connected to more than one poly entry");
          }
        }
        mc.CTfile = cm.getCTfileContents(pdbcode.toUpperCase(), pres);
        ret.add(mc);
      }

    }
    return ret;
  }

}
