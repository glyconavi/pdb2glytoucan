package org.glycoinfo.PDB2GlyTouCan.structure;

public enum AtomSiteGroupType {
  ATOM,
  HETATM;
}
