package org.glycoinfo.PDB2GlyTouCan.structure;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.util.AtomComparator;
import org.glycoinfo.PDB2GlyTouCan.util.BondComparator;
import org.glycoinfo.PDB2GlyTouCan.util.BondComparator_CTFile;
import org.glycoinfo.PDB2GlyTouCan.util.Format;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class ConnectedEntryModel {

  public ArrayList<Atom> atoms = new ArrayList<>();
  public ArrayList<Bond> bonds = new ArrayList<>();
  public ConnectedEntry parent = null;
  public HashMap<Atom, String> atom_rep = new HashMap<>();

  ConnectedEntryModel(ArrayList<Atom> a, ArrayList<Bond> b) {
    atoms.addAll(a);
    bonds.addAll(b);
  }

  public ConnectedEntry getParent() {
    return parent;
  }

  public void setParent(ConnectedEntry ce) {
    parent = ce;
  }

  public String getModelNum() {
    String ret = "";
    for (Atom a : atoms) {
      String ss = a.getAtom_site_pdbx_PDB_model_num();
      if (ret.length() != 0 && !ss.equals(ret)) {
        System.err.println("contamination of model id." + ret + " " + ss + " " + ret + " is used.");
      } else {
        ret = ss;
      }
    }
    return ret;

  }

  public HashSet<String> getAtomCodes() {
    HashSet<String> ret = new HashSet<>();
    for (Atom a : atoms) {
      if (!atom_rep.containsKey(a)) {
        atom_rep.put(a, String.format("%s#%s#%s#%s#%s", a.getAtom_site_label_atom_id(),
            a.getAtom_site_label_comp_id(), a.getAtom_site_label_asym_id(),
            a.getAtom_site_label_entity_id(), a.getAtom_site_label_seq_id())
        );
      }
      ret.add(atom_rep.get(a));

    }
    return ret;
  }

  public boolean sameWith(ConnectedEntryModel se) {
    HashSet<String> m = new HashSet<>(this.getAtomCodes());
    HashSet<String> t = new HashSet<>(se.getAtomCodes());

    if (m.size() != t.size()) {
      return false;
    }

    int si = m.size();
    m.retainAll(t);

    return m.size() == si;
  }

  /**
   * 含まれている原子の重複の無い全 AsymId のリストを返す
   */
  public ArrayList<String> getAllAsymIds() {
    HashSet<String> allasym = new HashSet<>();

    for (Atom satom : this.atoms) {
      allasym.add(satom.getAtom_site_label_asym_id());
    }
    ArrayList<String> sal = new ArrayList<>(allasym);
    Collections.sort(sal);
    return sal;
  }

  /**
   * 含まれている原子の重複の無い全 EntityId のリストを返す
   */
  public ArrayList<String> getAllEntityIds() {
    HashSet<String> allent = new HashSet<>();

    for (Atom satom : this.atoms) {
      allent.add(satom.getAtom_site_label_entity_id());
    }
    ArrayList<String> sal = new ArrayList<>(allent);
    Collections.sort(sal);
    return sal;
  }

  /**
   * asym+comp+seqid で重複の無い CompID のリストを返す
   */
  public ArrayList<String> getAllCompLabels() {
    HashSet<String> alllabel = new HashSet<>();
    for (Atom satom : this.atoms) {
      alllabel.add(
          satom.getAtom_site_label_asym_id() + ";" + satom.getAtom_site_label_comp_id() + ";"
              + satom.getAtom_site_label_seq_id());
      // asym と並びを合わせるために結合している
    }
    ArrayList<String> cal = new ArrayList<>(alllabel);
    StringBuffer sbb = new StringBuffer();
    Collections.sort(cal);
    return cal;
  }

  public String getCTfileContents(String pdbcode, PdbxStructModResidue parentresidue) {
    for (int ii = 0; ii < this.atoms.size(); ii++) {
      this.atoms.get(ii).setCtfile_id(ii + 1);
    }
    Collections.sort(this.bonds, new BondComparator_CTFile());
    Mol gmol = new Mol(new LinkedList<Atom>(this.atoms)
        , new LinkedList<Bond>(this.bonds));

    String additionalcode = "";
    if (true) {//
      ArrayList<String> sal = this.getAllAsymIds();
      Collections.sort(sal);
      StringBuffer sb = new StringBuffer();
      for (String saa : sal) {
        if (sb.length() > 0) {
          sb.append("_");
        }
        sb.append(saa);
      }
      ArrayList<String> cal = this.getAllCompLabels();
      StringBuffer sbb = new StringBuffer();
      Collections.sort(cal);
      for (String saa : cal) {
        if (sbb.length() > 0) {
          sbb.append("_");
        }
        sbb.append(saa.split(";")[1]);
      }
      additionalcode = "." + sb.toString() + "." + sbb.toString();
    }
    String modelid = String.valueOf(this.atoms.get(0).getAtom_site_pdbx_PDB_model_num());

    String ctitle = "";
    if (parentresidue != null) {
      ctitle = pdbcode.toUpperCase() + "." + this.atoms.get(0).getAtom_site_pdbx_PDB_model_num()
          + "." + parentresidue.getLabel_asym_id() + "." + parentresidue.getLabel_comp_id() + "."
          + parentresidue.getLabel_seq_id() + "." + parentresidue.getId() + additionalcode;
    } else {
      ctitle = pdbcode.toUpperCase() + "." + this.atoms.get(0).getAtom_site_pdbx_PDB_model_num()
          + "." + "none" + "." + "none" + "." + "none" + "." + "none" + additionalcode;
    }
    String cTitle = ctitle;
    return Format
        .molFile(new LinkedList<Atom>(this.atoms), new LinkedList<Bond>(this.bonds), ctitle);
  }


  public static ArrayList<ConnectedEntryModel> separate(List<Atom> a, List<Bond> b) {
    ArrayList<ConnectedEntryModel> ret = new ArrayList<>();
    ArrayList<Atom> att = new ArrayList<>(a);
    HashMap<Atom, Integer> atom_to_id = new HashMap<>();

    HashMap<Atom, ArrayList<Bond>> atom_to_bond = new HashMap<>();
    HashMap<Atom, Atom> parentatom = new HashMap<>();
    for (int ii = 0; ii < att.size(); ii++) {
      atom_to_id.put(att.get(ii), ii);
      parentatom.put(att.get(ii), att.get(ii));
      atom_to_bond.put(att.get(ii), new ArrayList<Bond>());
    }

    for (Bond bb : b) {

      atom_to_bond.get(bb.getFromAtom()).add(bb);
      atom_to_bond.get(bb.getToAtom()).add(bb);

      Atom fatom = getRootAtom(bb.getFromAtom(), parentatom);
      Atom tatom = getRootAtom(bb.getToAtom(), parentatom);
      if (atom_to_id.get(fatom) > atom_to_id.get(tatom)) {
        parentatom.put(fatom, tatom);
      } else {
        parentatom.put(tatom, fatom);
      }

    }
    HashMap<Atom, ConnectedEntryModel> group = new HashMap<>();
    HashSet<Atom> pp = new HashSet<>();

    for (Atom aa : att) {
      pp.add(getRootAtom(aa, parentatom));
    }
    for (Atom pa : pp) {
      ArrayList<Atom> rr = new ArrayList<>();
      HashSet<Bond> bb = new HashSet<>();
      for (Atom aa : att) {
        if (getRootAtom(aa, parentatom) == pa) {
          rr.add(aa);
          bb.addAll(atom_to_bond.get(aa));
        }
      }
      ArrayList<Bond> bbsorted = new ArrayList<>(bb);
      Collections.sort(rr, new AtomComparator());
      Collections.sort(bbsorted, new BondComparator());
      ret.add(new ConnectedEntryModel(rr, bbsorted));
    }

    int acou = 0;
    int bcou = 0;
    for (ConnectedEntryModel c : ret) {
      acou += c.atoms.size();
      bcou += c.bonds.size();
    }

    //ここでメッセージが出るとコードを変更したせいて不整合が出ている。
    if (acou != a.size()) {
      System.err.println(
          "Atom numbers are different ??? Please check the code. " + acou + " " + a.size());
    }
    if (bcou != b.size()) {
      System.err.println(
          "Bond numbers are different ??? Please check the code. " + bcou + " " + b.size());
    }

    return ret;
  }

  /**
   * 最もベースとなる原子を返す。
   */
  public static Atom getRootAtom(Atom a, HashMap<Atom, Atom> parentmap) {
    Atom p = parentmap.get(a);
    int loopcount = 0;
    while (p != parentmap.get(p)) {
      p = parentmap.get(p);
      loopcount++;
      if (loopcount > 10000) {
        throw new RuntimeException("Infinite loop?");
      }
    }
    return p;
  }
}
