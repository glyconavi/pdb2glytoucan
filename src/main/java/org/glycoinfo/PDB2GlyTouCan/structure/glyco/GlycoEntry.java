package org.glycoinfo.PDB2GlyTouCan.structure.glyco;

import org.glycoinfo.PDB2GlyTouCan.structure.ModelCassette;

import java.util.Collections;
import java.util.List;

public class GlycoEntry {

  private List<String> entity_ids;
  private List<String> asym_ids;
  private List<String> chem_comp_ids;
  private List<String> mod_residue_asym_ids;
  private List<String> pdbx_struct_mod_residue_ids;
  private List<ModelCassette> models;

  public GlycoEntry() {
    entity_ids = Collections.emptyList();
    asym_ids = Collections.emptyList();
    chem_comp_ids = Collections.emptyList();
    mod_residue_asym_ids = Collections.emptyList();
    pdbx_struct_mod_residue_ids = Collections.emptyList();
    models = Collections.emptyList();
  }

  public List<String> getEntity_ids() {
    return entity_ids;
  }

  public void setEntity_ids(List<String> entity_ids) {
    this.entity_ids = entity_ids;
  }

  public List<String> getAsym_ids() {
    return asym_ids;
  }

  public void setAsym_ids(List<String> asym_ids) {
    this.asym_ids = asym_ids;
  }

  public List<String> getChem_comp_ids() {
    return chem_comp_ids;
  }

  public void setChem_comp_ids(List<String> chem_comp_ids) {
    this.chem_comp_ids = chem_comp_ids;
  }

  public List<String> getMod_residue_asym_ids() {
    return mod_residue_asym_ids;
  }

  public void setMod_residue_asym_ids(List<String> mod_residue_asym_ids) {
    this.mod_residue_asym_ids = mod_residue_asym_ids;
  }

  public List<String> getPdbx_struct_mod_residue_ids() {
    return pdbx_struct_mod_residue_ids;
  }

  public void setPdbx_struct_mod_residue_ids(List<String> pdbx_struct_mod_residue_ids) {
    this.pdbx_struct_mod_residue_ids = pdbx_struct_mod_residue_ids;
  }

  public List<ModelCassette> getModels() {
    return models;
  }

  public void setModels(List<ModelCassette> models) {
    this.models = models;
  }

  public String entryLabel() {
    StringBuffer sb = new StringBuffer();

    for (String ss : asym_ids) {
      sb.append(ss);
      sb.append("_");
    }

    return sb.toString();
  }

}
