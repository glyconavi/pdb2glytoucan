package org.glycoinfo.PDB2GlyTouCan.structure.glyco;

import org.glycoinfo.PDB2GlyTouCan.io.model.GlyTouCan;
import org.glycoinfo.PDB2GlyTouCan.io.model.Protein;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.EntityPoly;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.structure.Atom;
import org.glycoinfo.PDB2GlyTouCan.structure.ConnectedEntry;
import org.glycoinfo.PDB2GlyTouCan.structure.ConnectedEntryModel;
import org.glycoinfo.PDB2GlyTouCan.structure.ModelCassette;
import org.glycoinfo.PDB2GlyTouCan.structure.PDBEntity;
import org.glycoinfo.PDB2GlyTouCan.structure.Proteindb;
import org.glycoinfo.PDB2GlyTouCan.util.GlycoEntryComparator;
import org.glycoinfo.PDB2GlyTouCan.util.ModelCassetteComparator;
import org.glycoinfo.PDB2GlyTouCan.util.NumStrComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static org.glycoinfo.PDB2GlyTouCan.util.StringUtils.atomModResidueLabel;
import static org.glycoinfo.PDB2GlyTouCan.util.StringUtils.modResidueLabel;

public class GlycoProtein {

  private List<String> atomSiteLabelAsymId;
  private List<ConnectedEntryModel> connectedEntries;
  private Protein protein;

  public List<String> getAtomSiteLabelAsymId() {
    return atomSiteLabelAsymId;
  }

  public void setAtomSiteLabelAsymId(List<String> atomSiteLabelAsymId) {
    this.atomSiteLabelAsymId = atomSiteLabelAsymId;
  }

  public List<ConnectedEntryModel> getConnectedEntries() {
    return connectedEntries;
  }

  public void setConnectedEntries(
      List<ConnectedEntryModel> connectedEntries) {
    this.connectedEntries = connectedEntries;
  }

  public void setProtein(Protein protein) {
    this.protein = protein;
  }

  public Protein getProtein() {
    return protein;
  }

  public GlyTouCan toJson() {
    List<PDBEntity> entities = new ArrayList<>();
    List<ConnectedEntry> connectedEntities = new ArrayList<>();

    combineModels(entities, connectedEntities);

    GlyTouCan json = new GlyTouCan();

    json.setPDB_id(protein.getPdbCode());
    json.setAtom_site_label_asym_id(new ArrayList<>(atomSiteLabelAsymId));

    for (PDBEntity pe : entities) {
      json.setProtein_db(pe.protein_db);
      json.setAr_pdbx_struct_mod_residue(pe.ar_pdbx_struct_mod_residue);
    }

    for (ConnectedEntry ce : connectedEntities) {
      json.glyco_entries.add(glycoEntry(ce));
    }

    json.glyco_entries.sort(new GlycoEntryComparator());

    return json;
  }

  private GlycoEntry glycoEntry(ConnectedEntry connectedEntity) {
    List<String> entityIds = new ArrayList<>();
    List<String> asymIds = new ArrayList<>();
    List<ModelCassette> models = new ArrayList<>();

    Map<String, String> chemCompMap = new TreeMap<>();
    for (ConnectedEntryModel cm : connectedEntity.models) {
      entityIds.addAll(cm.getAllEntityIds());
      asymIds.addAll(cm.getAllAsymIds());

      for (Atom aa : cm.atoms) {
        String dlab;
        if (aa.getAtom_site_label_seq_id() == null) {
          dlab = "9999999";
        } else if (aa.getAtom_site_label_seq_id().equals("null")) {
          dlab = "9999999";
        } else {
          dlab = "0000000" + aa.getAtom_site_label_seq_id();
        }
        if (aa.getAtom_site_label_seq_id() != null && aa.getAtom_site_label_seq_id().length() > 6) {
          throw new RuntimeException("Atom number is too big??? You should change the code.");
        }
        String dcode =
            aa.getAtom_site_label_asym_id() + dlab.substring(dlab.length() - 6);

        String key = String.format("%s;%s", dcode, aa.getAtom_site_label_comp_id());

        chemCompMap.putIfAbsent(key, aa.getAtom_site_label_comp_id());
      }

      models.add(modelCassette(connectedEntity, cm));
    }

    List<String> chemCompIds = new ArrayList<>(chemCompMap.values());

    return glycoEntry(connectedEntity.parentResidues, entityIds, asymIds, chemCompIds, models);
  }

  private ModelCassette modelCassette(ConnectedEntry connectedEntity, ConnectedEntryModel cm) {
    ModelCassette mc = new ModelCassette();

    PdbxStructModResidue parent = null;
    if (connectedEntity.parentResidues.size() > 0) {
      parent = connectedEntity.parentResidues.get(0);
    }

    mc.pdbx_PDB_model_num = cm.getModelNum();
    mc.CTfile = cm.getCTfileContents(protein.getPdbCode(), parent);

    return mc;
  }

  private GlycoEntry glycoEntry(List<PdbxStructModResidue> parentResidues,
                                List<String> entityIds,
                                List<String> asymIds,
                                List<String> saccharides,
                                List<ModelCassette> models) {
    GlycoEntry glycoEntry = new GlycoEntry();

    glycoEntry.setEntity_ids(entityIds
        .stream()
        .distinct()
        .sorted(new NumStrComparator())
        .collect(Collectors.toList()));

    glycoEntry.setAsym_ids(asymIds
        .stream()
        .distinct()
        .sorted()
        .collect(Collectors.toList()));

    glycoEntry.setChem_comp_ids(saccharides);

    glycoEntry.setMod_residue_asym_ids(parentResidues
        .stream()
        .map(PdbxStructModResidue::getLabel_asym_id)
        .collect(Collectors.toList()));

    glycoEntry.setPdbx_struct_mod_residue_ids(parentResidues
        .stream()
        .map(PdbxStructModResidue::getId)
        .collect(Collectors.toList()));

    glycoEntry.setModels(models.stream().sorted(new ModelCassetteComparator()).collect(Collectors.toList()));

    return glycoEntry;
  }

  private void combineModels(List<PDBEntity> entities, List<ConnectedEntry> connectedEntities) {
    if (getConnectedEntries().size() > 0) {

      for (ConnectedEntryModel ce : getConnectedEntries()) {
        ConnectedEntry cmm = null;

        for (ConnectedEntry c : connectedEntities) {
          if (c.sameWith(ce)) {
            c.addModel(ce);
            if (cmm == null) {
              cmm = c;
            } else {
              System.err.println("???? found duplicate entries.");
            }
          }
        }

        if (cmm == null) {
          ConnectedEntry ez = new ConnectedEntry();
          ez.addModel(ce);
          connectedEntities.add(ez);
        }
      }

      for (PdbxStructModResidue pp : protein.arPdbxStructModResidue()) {
        String smlabel = modResidueLabel(pp);
        for (ConnectedEntry ce : connectedEntities) {
          boolean matchflag = false;
          for (ConnectedEntryModel ss : ce.models) {
            for (Atom a : ss.atoms) {
              if (smlabel.equals(atomModResidueLabel(a))) {
                matchflag = true;
                break;
              }
            }
          }
          if (matchflag) {
            ce.addParentResidue(pp);
          }
        }
      }
    }

    List<String> idss = protein.getEntityPolies()
        .stream()
        .map(EntityPoly::getEntity_id)
        .distinct().sorted().collect(Collectors.toList());

    Map<String, Set<String>> entityToStructAsym = protein.entityToStructAsym();

    for (String id : idss) {
      List<String> asymss = new ArrayList<>(entityToStructAsym.get(id));
      Collections.sort(asymss);

      PDBEntity pe = new PDBEntity();
      pe.entity_ids_n.add(id);
      pe.asym_ids_n.addAll(asymss);

      entities.add(pe);
    }

    for (PDBEntity pe : entities) {
      HashSet<String> asymids = new HashSet<>(pe.asym_ids_n);
      HashSet<String> entids = new HashSet<>(pe.entity_ids_n);
      Iterator<ConnectedEntry> cite = connectedEntities.iterator();
      ArrayList<ConnectedEntry> childent = new ArrayList<>();

      while (cite.hasNext()) {
        ConnectedEntry cc = cite.next();
        outer:
        for (ConnectedEntryModel cmm : cc.models) {
          for (Atom a : cmm.atoms) {
            if (entids.contains(a.getAtom_site_label_entity_id())) {
              childent.add(cc);
              break outer;
            }
          }
        }
      }

      for (ConnectedEntry cc : childent) {
        pe.asym_ids_n.addAll(cc.models.get(0).getAllAsymIds());
        pe.entity_ids_n.addAll(cc.models.get(0).getAllEntityIds());
      }
      pe.asym_ids_n = new ArrayList<>(new HashSet<>(pe.asym_ids_n));
      pe.entity_ids_n = new ArrayList<>(new HashSet<>(pe.entity_ids_n));

      Collections.sort(pe.asym_ids_n);
      Collections.sort(pe.entity_ids_n);

      pe.model.addAll(ConnectedEntry.entryModelToCassette(protein.getPdbCode(), childent));

      for (Proteindb pp : protein.proteinDb()) {
        if (entids.contains(pp.getEntity_id())) {
          pe.protein_db.add(pp);
        }
      }
      for (PdbxStructModResidue pp : protein.arPdbxStructModResidue()) {
        if (asymids.contains(pp.getLabel_asym_id())) {
          pe.ar_pdbx_struct_mod_residue.add(pp);
        }
      }
    }

    Collections.sort(atomSiteLabelAsymId);
  }

}
