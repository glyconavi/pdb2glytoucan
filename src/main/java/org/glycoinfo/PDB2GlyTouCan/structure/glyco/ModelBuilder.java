package org.glycoinfo.PDB2GlyTouCan.structure.glyco;

import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2GlyTouCan.io.file.RemoteChemicalComponentLoader;
import org.glycoinfo.PDB2GlyTouCan.io.model.Protein;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.AtomSite;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructConn;
import org.glycoinfo.PDB2GlyTouCan.structure.Atom;
import org.glycoinfo.PDB2GlyTouCan.structure.AtomSiteGroupType;
import org.glycoinfo.PDB2GlyTouCan.structure.Bond;
import org.glycoinfo.PDB2GlyTouCan.structure.ChemicalComponentDictionary;
import org.glycoinfo.PDB2GlyTouCan.structure.ConnectedEntryModel;
import org.glycoinfo.PDB2GlyTouCan.util.FromAtomComparator;
import org.glycoinfo.PDB2GlyTouCan.util.MessageBuilder;
import org.glycoinfo.PDB2GlyTouCan.util.ToAtomComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.glycoinfo.PDB2GlyTouCan.util.StringUtils.modelCode;

public class ModelBuilder {

  private static final Logger logger = LoggerFactory.getLogger(ModelBuilder.class);

  private static final String[] ions = {"CA", "MG", "NA", "CL"};

  private Protein protein;
  private ChemicalComponentLoader ccLoader;

  private GlycoProtein glycoProtein;
  private List<String> atomSiteLabelAsymId;
  private List<ConnectedEntryModel> connectedEntries;
  private List<String> molCodes;

  private static List<String> ions() {
    return Arrays.asList(ions);
  }

  public ModelBuilder(Protein protein) {
    this(protein, new RemoteChemicalComponentLoader());
  }

  public ModelBuilder(Protein protein, ChemicalComponentLoader ccLoader) {
    this.protein = protein;
    this.ccLoader = ccLoader;
  }

  public GlycoProtein build() {
    if (this.glycoProtein != null) {
      return this.glycoProtein;
    }

    atomSiteLabelAsymId = new ArrayList<>();
    connectedEntries = new ArrayList<>();
    molCodes = protein.getAtomSites()
        .stream()
        .map(x -> molCode(protein.getPdbCode(), x.getPdbx_PDB_model_num()))
        .collect(Collectors.toList());

    molCodes.stream().distinct().sorted().forEach(code -> {
      logger.debug("Processing model: " + code);

      List<Atom> atoms = new LinkedList<>();
      List<Atom> saccharideAtoms = new LinkedList<>();
      List<Atom> allAtoms = new ArrayList<>();
      Map<String, Atom> atomcode_to_atom = new HashMap<>();

      List<String> saccharideIds = saccharideIds();

      for (AtomSite as : protein.getAtomSites()) {
        String molcode = molCode(protein.getPdbCode(), as.getPdbx_PDB_model_num());

        if (!code.equals(molcode)) {
          continue;
        }

        Atom atom = convertAtom(as);

        String uniqueCode = atom.getUniqueCode(as.getPdbx_PDB_ins_code());

        if (!atomcode_to_atom.containsKey(uniqueCode)) {
          atomcode_to_atom.put(uniqueCode, atom);
        } else {
          if (atomcode_to_atom.get(uniqueCode).getAtom_site_occupancy()
              < atom.getAtom_site_occupancy()) {
            atomcode_to_atom.put(uniqueCode, atom);
          }
        }

        allAtoms.add(atom);
      }

      removeLowOccupancyAtoms(allAtoms, atomcode_to_atom.values());

      int atom_count = 1;
      int saccharide_atom_count = 1;
      int ion_count = 1;

      for (Atom at : allAtoms) {
        if (at.getGroup_PDB().equals(AtomSiteGroupType.ATOM.toString())) {
          at.setCtfile_id(atom_count);
          atom_count++;
          atoms.add(at);
        } else if (at.getGroup_PDB().equals(AtomSiteGroupType.HETATM.toString())) {

          if (saccharideIds.contains(at.getAtom_site_label_comp_id())) {
            at.setCtfile_id(saccharide_atom_count);
            saccharide_atom_count++;
            saccharideAtoms.add(at);
          } else {
            at.setCtfile_id(atom_count);
            atom_count++;
            atoms.add(at);
          }

          if (ions().contains(at.getAtom_site_label_comp_id())) {
            at.setCtfile_id(ion_count);
            ion_count++;
          }
        }
      }

      List<Atom> saccharide_atoms_noaa = new LinkedList<>(saccharideAtoms);
      List<Bond> het_bonds = null;
      try {
        het_bonds = new ChemicalComponentDictionary(ccLoader).bondsFor(saccharideAtoms);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (MMjsonParseException e) {
        e.printStackTrace();
      }
      List<Bond> str_conn_bonds = new LinkedList<>();

      int add_atom_count = saccharideAtoms.size() + 1;

      HashMap<Atom, HashSet<Atom>> bondpair = new HashMap<>();

      HashMap<String, ArrayList<Atom>> atomcode_to_atoms = new HashMap<>();

      for (Atom att : atoms) {
        String attcode =
            att.getAtom_site_label_comp_id() + ";;;" + att.getAtom_site_label_atom_id() + ";;;"
                + att.getAtom_site_label_asym_id() + ";;;" + att.getAtom_site_label_seq_id();

        if (!atomcode_to_atoms.containsKey(attcode)) {
          atomcode_to_atoms.put(attcode, new ArrayList<Atom>());
        }
        atomcode_to_atoms.get(attcode).add(att);
      }

      for (int i = 0; i < structConnections().size(); i++) {
        Bond bd = new Bond();
        StructConn sconn = structConnections().get(i);
        Atom from_atom = null;
        Atom to_atom = null;

        String pt1code =
            sconn.getPtnr1_label_comp_id() + ";;;" + sconn.getPtnr1_label_atom_id() + ";;;" + sconn
                .getPtnr1_label_asym_id() + ";;;" + sconn.getPtnr1_label_seq_id();
        String pt2code =
            sconn.getPtnr2_label_comp_id() + ";;;" + sconn.getPtnr2_label_atom_id() + ";;;" + sconn
                .getPtnr2_label_asym_id() + ";;;" + sconn.getPtnr2_label_seq_id();

        for (Atom jatom : saccharide_atoms_noaa) {
          if (FromAtomComparator.equals(jatom, sconn, false)) {
            if (from_atom != null) {
              logger.warn(MessageBuilder.fromAtomDuplication(sconn, jatom, from_atom));
              continue;
            }
            from_atom = jatom;
          }
        }

        for (Atom jatom : saccharide_atoms_noaa) {
          if (ToAtomComparator.equals(jatom, sconn, false)) {
            if (to_atom != null) {
              logger.warn(MessageBuilder.toAtomDuplication(sconn, jatom, to_atom));
              continue;
            }
            to_atom = jatom;
          }
        }
        if (atomcode_to_atoms.containsKey(pt1code)) {
          ArrayList<Atom> atlis = atomcode_to_atoms.get(pt1code);
          if (from_atom != null || atlis.size() > 1) {
            logger.warn(MessageBuilder.fromAtomDuplication(sconn));
          } else {
            from_atom = atlis.get(0);
            from_atom.setCtfile_id(add_atom_count);
            saccharideAtoms.add(from_atom);
            add_atom_count++;
          }
        }
        if (atomcode_to_atoms.containsKey(pt2code)) {
          ArrayList<Atom> atlis = atomcode_to_atoms.get(pt2code);
          if (to_atom != null || atlis.size() > 1) {
            logger.warn(MessageBuilder.toAtomDuplication(sconn));
          } else {
            to_atom = atlis.get(0);
            to_atom.setCtfile_id(add_atom_count);
            saccharideAtoms.add(to_atom);
            add_atom_count++;
          }
        }

        if (from_atom == null) {
          logger.warn(MessageBuilder.fromAtomNotFound(sconn, protein.getPdbCode()));
          continue;
        }

        if (to_atom == null) {
          logger.warn(MessageBuilder.toAtomNotFound(sconn, protein.getPdbCode()));
          continue;
        }

        bd.setFromAtom(from_atom);
        bd.setToAtom(to_atom);
        bd.setBondOrder(1);

        if (!bondpair.containsKey(from_atom)) {
          bondpair.put(from_atom, new HashSet<>());
        }

        if (!bondpair.containsKey(to_atom)) {
          bondpair.put(to_atom, new HashSet<>());
        }

        if (bondpair.get(from_atom).contains(to_atom)
            || bondpair.get(to_atom).contains(from_atom)) {
          continue;
        }

        bondpair.get(from_atom).add(to_atom);
        bondpair.get(to_atom).add(from_atom);
        str_conn_bonds.add(bd);
      }

      // remove duplicated atoms
      HashSet<Atom> atomfilt = new HashSet<>();
      Iterator<Atom> ati = saccharideAtoms.iterator();
      while (ati.hasNext()) {
        Atom att = ati.next();
        if (atomfilt.contains(att)) {
          ati.remove();
          logger.warn(MessageBuilder.multipleBond(att));
        }
        atomfilt.add(att);
      }

      het_bonds.addAll(str_conn_bonds);

      for (Atom at : saccharideAtoms) {
        if (!atomSiteLabelAsymId.contains(at.getAtom_site_label_asym_id())) {
          atomSiteLabelAsymId.add(at.getAtom_site_label_asym_id());
        }
      }
      if (saccharideAtoms.size() == 0) {
        return;
      }

      List<ConnectedEntryModel> sep = ConnectedEntryModel.separate(saccharideAtoms, het_bonds);
      connectedEntries.addAll(sep);
    });

    GlycoProtein glycoProtein = new GlycoProtein();

    glycoProtein.setAtomSiteLabelAsymId(atomSiteLabelAsymId);
    glycoProtein.setConnectedEntries(connectedEntries);
    glycoProtein.setProtein(protein);

    this.glycoProtein = glycoProtein;

    return this.glycoProtein;
  }

  private void removeLowOccupancyAtoms(List<Atom> source, Collection<Atom> atoms) {
    Set<Atom> set = new HashSet<>(atoms);

    if (set.size() != source.size()) {
      source.retainAll(set);
    }

    if (source.size() != (new HashSet<>(source)).size()) {
      throw new RuntimeException("Duplicated atoms found");
    }
  }

  private Atom convertAtom(AtomSite atom) {
    Atom a = new Atom();

    a.setAtom_id(atom.getId());
    a.setAtom_site_label_entity_id(atom.getLabel_entity_id());
    a.setAtom_site_label_asym_id(atom.getLabel_asym_id());
    a.setAtom_site_label_atom_id(atom.getLabel_atom_id());
    a.setAtom_site_label_comp_id(atom.getLabel_comp_id());
    a.setAtom_site_label_seq_id(atom.getLabel_seq_id());
    a.setSeq_id(atom.getLabel_seq_id());
    a.setX(Double.parseDouble(atom.getCartn_x()));
    a.setY(Double.parseDouble(atom.getCartn_y()));
    a.setZ(Double.parseDouble(atom.getCartn_z()));
    a.setAtom_site_type_symbol(atom.getType_symbol());
    a.setGroup_PDB(atom.getGroup_PDB());
    a.setComp_id(atom.getLabel_comp_id());
    a.setAtom_site_id(atom.getId());
    a.setAtom_site_pdbx_PDB_model_num(atom.getPdbx_PDB_model_num());

    double occupancy;
    try {
      occupancy = Double.parseDouble(atom.getOccupancy());
    } catch (NumberFormatException e) {
      occupancy = 0.0;
    }
    a.setAtom_site_occupancy(occupancy);

    return a;
  }

  private List<StructConn> structConnections() {
    List<String> saccharideIds = saccharideIds();

    return protein
        .getStructConns()
        .stream()
        .filter(sc -> saccharideIds.contains(sc.getPtnr1_label_comp_id())
            || saccharideIds.contains(sc.getPtnr2_label_comp_id()))
        .collect(Collectors.toList());
  }

  private List<String> saccharideIds() {
    return ccLoader.saccharideIds();
  }

  private String molCode(String pdbCode, String pdbModelNum) {
    return String.format("%s.%s", pdbCode.toUpperCase(), modelCode(pdbModelNum));
  }

}
