package org.glycoinfo.PDB2GlyTouCan.structure;

/**
 * For standard polymer components, the type of the monomer. Note that monomers that will form
 * polymers are of three types: linking monomers, monomers with some type of N-terminal (or 5') cap
 * and monomers with some type of C-terminal (or 3') cap.
 */
public enum ChemicalComponentType {

  D_PEPTIDE_COOH_CARBOXY_TERMINUS("D-peptide COOH carboxy terminus "),
  D_PEPTIDE_NH3_AMINO_TERMINUS("D-peptide NH3 amino terminus  "),
  D_PEPTIDE_LINKING("D-peptide linking "),
  D_SACCHARIDE("D-saccharide  "),
  D_SACCHARIDE_1_4_AND_1_4_LINKING("D-saccharide 1,4 and 1,4 linking  "),
  D_SACCHARIDE_1_4_AND_1_6_LINKING("D-saccharide 1,4 and 1,6 linking  "),
  DNA_OH_3_PRIME_TERMINUS("DNA OH 3 prime terminus "),
  DNA_OH_5_PRIME_TERMINUS("DNA OH 5 prime terminus "),
  DNA_LINKING("DNA linking "),
  L_PEPTIDE_COOH_CARBOXY_TERMINUS("L-peptide COOH carboxy terminus "),
  L_PEPTIDE_NH3_AMINO_TERMINUS("L-peptide NH3 amino terminus  "),
  L_PEPTIDE_LINKING("L-peptide linking "),
  L_SACCHARIDE("L-saccharide  "),
  L_SACCHARIDE_1_4_AND_1_4_LINKING("L-saccharide 1,4 and 1,4 linking  "),
  L_SACCHARIDE_1_4_AND_1_6_LINKING("L-saccharide 1,4 and 1,6 linking  "),
  RNA_OH_3_PRIME_TERMINUS("RNA OH 3 prime terminus "),
  RNA_OH_5_PRIME_TERMINUS("RNA OH 5 prime terminus "),
  RNA_LINKING("RNA linking "),
  NON_POLYMER("non-polymer "),
  OTHER("other "),
  SACCHARIDE("saccharide");

  private final String label;

  ChemicalComponentType(final String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return label;
  }
}
