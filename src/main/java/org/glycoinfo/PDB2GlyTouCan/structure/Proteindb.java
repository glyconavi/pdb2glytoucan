package org.glycoinfo.PDB2GlyTouCan.structure;

public class Proteindb {

  private String id;
  private String strand_id;
  private String entity_id;
  private String type;
  private String pdbx_seq_one_letter_code_can;

  /**
   * @return id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id セットする id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return strand_id
   */
  public String getStrand_id() {
    return strand_id;
  }

  /**
   * @param strand_id セットする strand_id
   */
  public void setStrand_id(String strand_id) {
    this.strand_id = strand_id;
  }

  /**
   * @return entity_id
   */
  public String getEntity_id() {
    return entity_id;
  }

  /**
   * @param entity_id セットする entity_id
   */
  public void setEntity_id(String entity_id) {
    this.entity_id = entity_id;
  }

  /**
   * @return type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type セットする type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return pdbx_seq_one_letter_code_can
   */
  public String getPdbx_seq_one_letter_code_can() {
    return pdbx_seq_one_letter_code_can;
  }

  /**
   * @param pdbx_seq_one_letter_code_can セットする pdbx_seq_one_letter_code_can
   */
  public void setPdbx_seq_one_letter_code_can(String pdbx_seq_one_letter_code_can) {
    this.pdbx_seq_one_letter_code_can = pdbx_seq_one_letter_code_can;
  }

}
