package org.glycoinfo.PDB2GlyTouCan.structure;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;

import java.util.ArrayList;
import java.util.LinkedList;

public class PDBEntity {

  public ArrayList<String> asym_ids_n = new ArrayList<>();
  public ArrayList<String> entity_ids_n = new ArrayList<>();
  public ArrayList<ModelCassette> model = new ArrayList<>();
  public LinkedList<Proteindb> protein_db = new LinkedList<Proteindb>();

  // FIXME: 本来は Asym 以下にあるべきだが山田さんが Entry に入れていたのでここにある。
  public LinkedList<PdbxStructModResidue> ar_pdbx_struct_mod_residue = new LinkedList<PdbxStructModResidue>();
}
