package org.glycoinfo.PDB2GlyTouCan.cli;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.apache.jena.rdf.model.Model;
import org.glycoinfo.PDB2GlyTouCan.io.model.GlyTouCan;
import org.glycoinfo.PDB2GlyTouCan.rdf.GlyTouCanModelBuilder;

import java.io.BufferedWriter;
import java.io.IOException;

public enum OutputFormat {

  JSON,
  RDF_TURTLE;

  public String fileName(String code) {
    switch (this) {
      case JSON:
        return String.format("%s.json.gz", code.toLowerCase());
      case RDF_TURTLE:
        return String.format("%s.ttl.gz", code.toLowerCase());
      default:
        return code.toLowerCase();
    }
  }

  public void output(BufferedWriter out, GlyTouCan json) throws IOException {
    switch (this) {
      case JSON:
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        writer.writeValue(out, json);
        break;
      case RDF_TURTLE:
        GlyTouCanModelBuilder rdfBuilder = new GlyTouCanModelBuilder();
        Model model = rdfBuilder.entry(json).build();
        model.write(out, "TURTLE", GlyTouCanModelBuilder.BASE);
        break;
    }
  }
}
