package org.glycoinfo.PDB2GlyTouCan.cli;

import org.glycoinfo.PDB2GlyTouCan.exception.MMjsonParseException;
import org.glycoinfo.PDB2GlyTouCan.io.PdbJsonParser;
import org.glycoinfo.PDB2GlyTouCan.io.file.ChemicalComponentLoader;
import org.glycoinfo.PDB2GlyTouCan.io.file.Files;
import org.glycoinfo.PDB2GlyTouCan.io.file.LocalChemicalComponentLoader;
import org.glycoinfo.PDB2GlyTouCan.io.file.RemoteChemicalComponentLoader;
import org.glycoinfo.PDB2GlyTouCan.io.model.GlyTouCan;
import org.glycoinfo.PDB2GlyTouCan.io.model.Protein;
import org.glycoinfo.PDB2GlyTouCan.structure.glyco.GlycoProtein;
import org.glycoinfo.PDB2GlyTouCan.structure.glyco.ModelBuilder;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Paths;

import static org.glycoinfo.PDB2GlyTouCan.io.file.Files.newBufferedWriter;
import static org.glycoinfo.PDB2GlyTouCan.util.StringUtils.formatTime;

public class Converter {

  private static final Logger logger = LoggerFactory.getLogger(Converter.class);

  private String output;

  private boolean noNetwork;

  private String ccPath;

  private String pdbPath;

  private String pdbCode;

  private boolean force;

  private OutputFormat format;

  public Converter() {
    this.format = OutputFormat.JSON;
  }

  public Converter output(String output) {
    this.output = output;
    return this;
  }

  public Converter noNetwork(boolean noNetwork) {
    this.noNetwork = noNetwork;
    return this;
  }

  public Converter ccPath(String ccPath) {
    this.ccPath = ccPath;
    return this;
  }

  public Converter pdbPath(String pdbPath) {
    this.pdbPath = pdbPath;
    return this;
  }

  public Converter pdbCode(String pdbCode) {
    this.pdbCode = pdbCode;
    return this;
  }

  public Converter force(boolean force) {
    this.force = force;
    return this;
  }

  public Converter format(OutputFormat format) {
    this.format = format;
    return this;
  }

  public void run() {
    String inputFile = String.format("%s.json.gz", pdbCode.toLowerCase());

    if (noNetwork) {
      try (Reader reader = Files.newBufferedReader(Paths.get(pdbPath, inputFile).toFile())) {
        process(reader, new LocalChemicalComponentLoader(ccPath));
      } catch (IOException | MMjsonParseException e) {
        e.printStackTrace();
      }
    } else {
      try (Reader reader = Files.newBufferedReader(new URL("ftp://ftp.pdbj.org/mine2/data/mmjson/all/" + inputFile))) {
        process(reader, new RemoteChemicalComponentLoader());
      } catch (IOException | MMjsonParseException e) {
        e.printStackTrace();
      }
    }
  }

  private void process(Reader reader, ChemicalComponentLoader ccLoader) throws IOException, MMjsonParseException {
    long start = System.currentTimeMillis();

    PdbJsonParser parser = new PdbJsonParser(reader);

    Protein protein = parser.parse();
    ModelBuilder builder = new ModelBuilder(protein, ccLoader);

    GlycoProtein gp = builder.build();
    GlyTouCan json = gp.toJson();

    if (json.getGlyco_entries().isEmpty()) {
      logger.info(pdbCode + " has no glyco entries");
      return;
    }

    String time = formatTime(System.currentTimeMillis() - start);
    json.setTime(time);
    json.setDate(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));

    try (BufferedWriter bw = newBufferedWriter(outFile())) {
      format.output(bw, json);
    }

    logger.info(pdbCode + " finished in " + time);
  }

  private File outFile() {
    File path;
    if (output == null) {
      path = new File(".");
    } else {
      path = new File(output);
    }

    if (path.isDirectory()) {
      path = Paths.get(path.toString(), format.fileName(pdbCode.toLowerCase())).toFile();
    }

    if (!force && path.exists()) {
      throw new RuntimeException("File already exists");
    }

    return path;
  }

}
