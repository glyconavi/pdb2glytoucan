package org.glycoinfo.PDB2GlyTouCan.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.PrintWriter;

public class App {

  static final String APP_NAME = "pdb2glytoucan";
  static final String APP_VERSION = "0.9.0";

  private static final int STATUS_PARSE_ERROR = 1;
  private static final int STATUS_ARGUMENT_ERROR = 2;

  private static final String OPTION_NO_NETWORK_LONG = "no-network";
  private static final String OPTION_PDB_PATH_LONG = "pdb";
  private static final String OPTION_CC_PATH_LONG = "cc";
  private static final String OPTION_OUTPUT_SHORT = "o";
  private static final String OPTION_OUTPUT_LONG = "output";
  private static final String OPTION_FORMAT_SHORT = "f";
  private static final String OPTION_FORMAT_LONG = "format";

  public static void main(String[] args) {
    new App().run(args);
  }

  void run(String[] args) {
    CommandLineParser parser = new DefaultParser();

    try {
      CommandLine cmd = parser.parse(options(), args);

      if (cmd.getArgList().isEmpty()) {
        throw new IllegalArgumentException("PDB code");
      }

      String pdbCode = cmd.getArgList().get(0);

      if (cmd.hasOption(OPTION_NO_NETWORK_LONG)) {
        if (!(cmd.hasOption(OPTION_CC_PATH_LONG) && cmd.hasOption(OPTION_PDB_PATH_LONG))) {
          throw new IllegalArgumentException("both of --cc <DIRECTORY> and --pdb <DIRECTORY> are required");
        }
      }

      OutputFormat format = OutputFormat.JSON;
      if (cmd.hasOption(OPTION_FORMAT_LONG)) {
        String opt = cmd.getOptionValue(OPTION_FORMAT_LONG).toLowerCase();
        if (opt.equals("ttl") || opt.equals("turtle")) {
          format = OutputFormat.RDF_TURTLE;
        } else {
          throw new IllegalArgumentException("Unknown format");
        }
      }

      new Converter()
          .output(cmd.getOptionValue(OPTION_OUTPUT_SHORT))
          .noNetwork(cmd.hasOption(OPTION_NO_NETWORK_LONG))
          .ccPath(cmd.getOptionValue(OPTION_CC_PATH_LONG))
          .pdbPath(cmd.getOptionValue(OPTION_PDB_PATH_LONG))
          .format(format)
          .pdbCode(pdbCode)
          .run();

    } catch (ParseException e) {
      printUsage();
      System.exit(STATUS_PARSE_ERROR);
    } catch (IllegalArgumentException e) {
      System.err.println("Argument Error: " + e.getMessage() + "\n");
      printUsage();
      System.exit(STATUS_ARGUMENT_ERROR);
    }
  }

  private void printUsage() {
    String syntax = String
        .format("%s [%s] [%s <DIRECTORY>] [%s <DIRECTORY>] [%s <DIRECTORY>] [PDB code ...]",
            APP_NAME, OPTION_NO_NETWORK_LONG, OPTION_CC_PATH_LONG, OPTION_PDB_PATH_LONG,
            OPTION_OUTPUT_SHORT);
    String header = "\nOptions:";
    String footer = String.format("\nVersion: %s", APP_VERSION);

    HelpFormatter hf = new HelpFormatter();

    hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
        header, options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
  }

  private Options options() {
    Options options = new Options();

    options.addOption(Option.builder()
        .longOpt(OPTION_NO_NETWORK_LONG)
        .desc("Use local data, both of --cc <DIRECTORY> and --pdb <DIRECTORY> are required")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_CC_PATH_LONG)
        .desc("Path to the directory where PDB Chemical Component Dictionary is placed")
        .hasArg()
        .argName("DIRECTORY")
        .build()
    );

    options.addOption(Option.builder()
        .longOpt(OPTION_PDB_PATH_LONG)
        .desc("Path to the directory where PDB Chemical Component Model getData file is placed")
        .hasArg()
        .argName("DIRECTORY")
        .build()
    );

    options.addOption(Option.builder(OPTION_OUTPUT_SHORT)
        .longOpt(OPTION_OUTPUT_LONG)
        .desc("Path to output directory")
        .hasArg()
        .argName("DIRECTORY")
        .build()
    );

    options.addOption(Option.builder(OPTION_FORMAT_SHORT)
        .longOpt(OPTION_FORMAT_LONG)
        .desc("Output format [json|turtle(ttl)]")
        .hasArg()
        .argName("FORMAT")
        .required()
        .build()
    );

    options.addOption(Option.builder("v")
        .longOpt("version")
        .desc("Print version information")
        .build()
    );

    options.addOption(Option.builder("h")
        .longOpt("help")
        .desc("Show usage help")
        .build()
    );

    return options;
  }

}
