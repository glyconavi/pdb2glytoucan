package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructConn;
import org.glycoinfo.PDB2GlyTouCan.structure.Atom;

public class ToAtomComparator {

  public static boolean equals(Atom att, StructConn sconn, boolean checkseqid) {
    if (att.getAtom_site_label_comp_id().equals(sconn.getPtnr2_label_comp_id())) {
      if (att.getAtom_site_label_atom_id().equals(sconn.getPtnr2_label_atom_id())) {
        if (att.getAtom_site_label_asym_id().equals(sconn.getPtnr2_label_asym_id())) {
          if (!checkseqid) {
            return true;
          } else {
            return att.getAtom_site_label_seq_id().equals(sconn.getPtnr2_label_seq_id());
          }
        }
      }
    }
    return false;
  }

}
