package org.glycoinfo.PDB2GlyTouCan.util;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class Benchmark {

  public static double realTime(Runnable r) {
    Benchmark bm = new Benchmark();

    bm.start();

    r.run();

    bm.stop();

    return bm.realTime();
  }

  private boolean processing = false;
  private DateTime start;
  private DateTime stop;

  public void start() {
    processing = true;
    start = new DateTime();
  }

  public void stop() {
    processing = false;
    stop = new DateTime();
  }

  public double realTime() {
    if (processing) {
      stop();
    }

    if (start == null || stop == null) {
      return 0d;
    }

    Duration duration = new Duration(start, stop);

    PeriodFormatter formatter = new PeriodFormatterBuilder()
        .appendSecondsWithMillis()
        .toFormatter();

    try {
      return Double.parseDouble(formatter.print(duration.toPeriod()));
    } catch (NumberFormatException e) {
      return 0d;
    }
  }
}
