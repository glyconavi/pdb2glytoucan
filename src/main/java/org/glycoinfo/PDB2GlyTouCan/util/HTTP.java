package org.glycoinfo.PDB2GlyTouCan.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

public class HTTP {

  public static String httpGet(String urlString) throws IOException {
    return httpGet(urlString, null);
  }

  public static String httpGet(String urlString, String contentType) throws IOException {
    HttpURLConnection con = null;

    try {
      URL url = new URL(urlString);

      con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      if (contentType != null) {
        con.setRequestProperty("Content-Type", contentType);
      }

      con.connect();

      int status = con.getResponseCode();

      if (status == HttpURLConnection.HTTP_OK) {
        if (url.getFile().endsWith(".gz")) {
          return readResponse(con, new GZIPInputStream(con.getInputStream()));
        } else {
          return readResponse(con, con.getInputStream());
        }
      } else {
        return null;
      }
    } finally {
      if (con != null) {
        con.disconnect();
      }
    }
  }

  private static String readResponse(URLConnection con, InputStream in) throws IOException {
    String encoding = con.getContentEncoding();
    if (encoding == null) {
      encoding = StandardCharsets.UTF_8.name();
    }

    try (InputStreamReader isr = new InputStreamReader(in, encoding);
         BufferedReader br = new BufferedReader(isr)) {

      return br.lines().reduce("", (a, b) -> a + b + System.getProperty("line.separator"));
    }
  }
}
