package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.structure.Atom;

public class AtomComparator implements java.util.Comparator<Atom> {

  public int compare(Atom a1, Atom a2) {
    int i1 = 0;
    int i2 = 0;
    try {
      i1 = Integer.parseInt(a1.getAtom_site_id());
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }
    try {
      i2 = Integer.parseInt(a2.getAtom_site_id());
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }

    return Integer.compare(i1, i2);
  }
}
