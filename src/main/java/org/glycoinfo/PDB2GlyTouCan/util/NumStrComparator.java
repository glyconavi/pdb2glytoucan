package org.glycoinfo.PDB2GlyTouCan.util;

public class NumStrComparator implements java.util.Comparator<String> {

  public int compare(String a1, String a2) {
    double f1 = 100000;
    double f2 = 100000;

    try {
      f2 = Double.parseDouble(a2);
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
      return -1;
    }
    try {
      f1 = Double.parseDouble(a1);
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
      return 1;
    }

    return Double.compare(f1, f2);
  }
}
