package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.structure.Atom;
import org.glycoinfo.PDB2GlyTouCan.structure.Bond;
import org.joda.time.DateTime;

import java.util.LinkedList;

public class Format {

  public static String molFile(LinkedList<Atom> atoms, LinkedList<Bond> bonds, String id) {
    StringBuilder sb = new StringBuilder();

    String dtsting = DateTime.now().toString("MMddyyHHmm");

    sb.append(id);
    sb.append("\n");
    sb.append(String.format("mmJSON %s3D\n", dtsting));
    sb.append(String.format("GlycoNAVI: GlycoSite atom:%s bond:%s\n", atoms.size(), bonds.size()));
    sb.append(String.format("%3s%3s  0  0  0  0  0  0  0  0999 V2000\n", atoms.size(), bonds.size()));

    for (Atom at : atoms) {
      sb.append(String.format("%10.4f%10.4f%10.4f %-2s  0  0  0  0  0  0  0  0  0  0  0  0\n",
          at.getX(), at.getY(), at.getZ(), at.getAtom_site_type_symbol()));
    }

    for (Bond bond : bonds) {
      sb.append(String.format("%3s%3s%3s%3s  0  0  0\n", bond.getFromAtom().getCtfile_id(),
          bond.getToAtom().getCtfile_id(), bond.getBondOrder(), 0));
    }

    sb.append("M  END\n");

    return sb.toString();
  }
}
