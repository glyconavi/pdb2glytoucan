package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.structure.Bond;

public class BondComparator_CTFile implements java.util.Comparator<Bond> {

  public int compare(Bond a1, Bond a2) {
    int i1 = a1.getFromAtom().getCtfile_id();
    int i2 = a2.getFromAtom().getCtfile_id();

    if (i1 < i2) {
      return -1;
    }
    if (i1 > i2) {
      return 1;
    }

    i1 = a1.getToAtom().getCtfile_id();
    i2 = a2.getToAtom().getCtfile_id();

    return Integer.compare(i1, i2);
  }
}
