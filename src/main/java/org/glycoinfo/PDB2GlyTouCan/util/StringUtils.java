package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.structure.Atom;

import static com.google.common.base.Strings.padStart;

public class StringUtils {

  public static String formatTime(long milliSeconds) {
    long sec = milliSeconds / 1000;
    long min = (sec % 3600) / 60;
    long hour = sec / 3600;

    return String.format("%2s h %2s m %2s s", hour, min, sec % 60);
  }

  public static String modelCode(String pdbModelNum) {
    return padStart(pdbModelNum, 7, '0');
  }

  public static String modResidueLabel(PdbxStructModResidue obj) {
    return codeFormat(obj.getLabel_asym_id(), obj.getLabel_comp_id(), obj.getLabel_seq_id());
  }

  public static String atomModResidueLabel(Atom obj) {
    return codeFormat(obj.getAtom_site_label_asym_id(), obj.getAtom_site_label_comp_id(),
        obj.getAtom_site_label_seq_id());
  }

  public static String codeFormat(String s1, String s2, String s3) {
    return String.format("***%s###%s###%s***", s1, s2, s3);
  }

  public static String codeFormat(String s1, String s2, String s3, String s4) {
    return String.format("***%s###%s###%s###%s***", s1, s2, s3, s4);
  }

}
