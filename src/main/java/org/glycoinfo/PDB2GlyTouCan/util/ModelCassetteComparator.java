package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.structure.ModelCassette;

public class ModelCassetteComparator implements java.util.Comparator<ModelCassette> {

  public int compare(ModelCassette a1, ModelCassette a2) {
    int i1 = 0;
    int i2 = 0;

    try {
      i1 = Integer.parseInt(a1.pdbx_PDB_model_num);
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }

    try {
      i2 = Integer.parseInt(a2.pdbx_PDB_model_num);
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }

    return Integer.compare(i1, i2);
  }
}
