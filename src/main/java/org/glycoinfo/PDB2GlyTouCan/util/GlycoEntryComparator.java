package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.structure.glyco.GlycoEntry;

public class GlycoEntryComparator implements java.util.Comparator<GlycoEntry> {

  public int compare(GlycoEntry a1, GlycoEntry a2) {
    return a1.entryLabel().compareTo(a2.entryLabel());
  }
}
