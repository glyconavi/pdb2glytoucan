package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.io.model.protein.StructConn;
import org.glycoinfo.PDB2GlyTouCan.structure.Atom;

import static org.glycoinfo.PDB2GlyTouCan.util.StringUtils.codeFormat;

public class MessageBuilder {

  public static String fromAtomDuplication(StructConn sc) {
    return duplicationMessage(sc, fromAtomCode(sc), null, null);
  }

  public static String fromAtomDuplication(StructConn sc, Atom atom, Atom fromAtom) {
    return duplicationMessage(sc, fromAtomCode(sc), atom, fromAtom);
  }

  public static String toAtomDuplication(StructConn sc) {
    return duplicationMessage(sc, toAtomCode(sc), null, null);
  }

  public static String toAtomDuplication(StructConn sc, Atom atom, Atom toAtom) {
    return duplicationMessage(sc, toAtomCode(sc), atom, toAtom);
  }

  public static String fromAtomNotFound(StructConn sc, String pdbCode) {
    return String.format("%s: From atom %s not found", pdbCode, fromAtomCode(sc));
  }

  public static String toAtomNotFound(StructConn sc, String pdbCode) {
    return String.format("%s: To atom %s not found", pdbCode, toAtomCode(sc));
  }

  public static String multipleBond(Atom att) {
    return String.format("Multiple bond: %s", atomCode(att));
  }

  private static String duplicationMessage(StructConn sc, String code, Atom atom1, Atom atom2) {
    StringBuilder sb = new StringBuilder();

    sb.append("Atom duplication?");
    sb.append("\n");
    sb.append(code);
    if (atom1 != null) {
      sb.append("\n");
      sb.append("this -> ");
      sb.append(atomCode(atom1));
    }
    if (atom2 != null) {
      sb.append("\n");
      sb.append("another -> ");
      sb.append(atomCode(atom2));
    }

    return sb.toString();
  }

  private static String notFoundMessage(String code, String pdbCode) {
    return String.format("%s: From atom %s not found", pdbCode, code);
  }

  private static String fromAtomCode(StructConn sc) {
    return codeFormat(sc.getPtnr1_label_asym_id(), sc.getPtnr1_label_atom_id(),
        sc.getPtnr1_label_comp_id(), sc.getPtnr1_label_seq_id());
  }

  private static String toAtomCode(StructConn sc) {
    return codeFormat(sc.getPtnr2_label_asym_id(), sc.getPtnr2_label_atom_id(),
        sc.getPtnr2_label_comp_id(), sc.getPtnr2_label_seq_id());
  }

  private static String atomCode(Atom at) {
    return codeFormat(at.getAtom_site_label_asym_id(), at.getAtom_site_label_atom_id(),
        at.getAtom_site_label_comp_id(), at.getAtom_site_label_seq_id());
  }
}
