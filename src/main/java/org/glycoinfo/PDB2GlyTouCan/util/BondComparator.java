package org.glycoinfo.PDB2GlyTouCan.util;

import org.glycoinfo.PDB2GlyTouCan.structure.Bond;

public class BondComparator implements java.util.Comparator<Bond> {

  public int compare(Bond a1, Bond a2) {
    int i1 = 0;
    int i2 = 0;
    try {
      i1 = Integer.parseInt(a1.getFromAtom().getAtom_site_id());
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }
    try {
      i2 = Integer.parseInt(a1.getFromAtom().getAtom_site_id());
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }
    if (i1 < i2) {
      return -1;
    }
    if (i1 > i2) {
      return 1;
    }

    try {
      i1 = Integer.parseInt(a1.getToAtom().getAtom_site_id());
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }
    try {
      i2 = Integer.parseInt(a1.getToAtom().getAtom_site_id());
    } catch (NumberFormatException exx) {
      exx.printStackTrace();
    }

    return Integer.compare(i1, i2);
  }
}
