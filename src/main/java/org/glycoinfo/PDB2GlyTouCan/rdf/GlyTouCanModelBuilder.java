package org.glycoinfo.PDB2GlyTouCan.rdf;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;
import org.glycoinfo.PDB2GlyTouCan.io.model.GlyTouCan;
import org.glycoinfo.PDB2GlyTouCan.io.model.protein.PdbxStructModResidue;
import org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary.GLYCAN;
import org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary.GLYTOUCAN;
import org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary.PDBO;
import org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary.SIO;
import org.glycoinfo.PDB2GlyTouCan.structure.EntityPolyType;
import org.glycoinfo.PDB2GlyTouCan.structure.Proteindb;
import org.glycoinfo.PDB2GlyTouCan.structure.glyco.GlycoEntry;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class GlyTouCanModelBuilder {

  public static final String BASE = "http://glycoinfo.org/pdb/";
  private static final String WWPDB_PREFIX = "https://rdf.wwpdb.org/pdb/";
  private static final String WWPDB_CC_PREFIX = "https://rdf.wwpdb.org/cc/";
  private static final String UNIPROT_PREFIX = "http://purl.uniprot.org/uniprot/";

  private GlyTouCan entry;

  public GlyTouCanModelBuilder entry(GlyTouCan entry) {
    this.entry = entry;

    return this;
  }

  public Model build() {
    Model model = namespacedModel();

    if (entry == null) {
      return model;
    }

    Resource uri = model.createResource(BASE + entry.getPDB_id());

    Set<String> residueDetails = entry.getAr_pdbx_struct_mod_residue().stream()
        .map(PdbxStructModResidue::getDetails)
        .collect(Collectors.toSet());

    if (residueDetails.contains("GLYCOSYLATION SITE")) {
      uri.addProperty(RDF.type, GLYCAN.Glycoprotein);
    } else {
      uri.addProperty(RDF.type, GLYCAN.Protein);
    }

    uri.addProperty(DCTerms.identifier, entry.getPDB_id());

    DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(entry.getDate());
    uri.addProperty(DCTerms.created, model.createTypedLiteral(dateTime.toCalendar(Locale.getDefault())));

    uri.addProperty(SKOS.closeMatch, model.createResource(WWPDB_PREFIX + entry.getPDB_id()));

    entry.getAtom_site_label_asym_id().forEach(x ->
        uri.addProperty(GLYTOUCAN.atom_site_label_asym_id, x)
    );

    addProtein_db(model, uri);
    addAr_pdbx_struct_mod_residue(model, uri);
    addGlycan(model, uri);

    return model;
  }

  private void addProtein_db(Model model, Resource uri) {
    for (Proteindb db : entry.getProtein_db()) {
      Resource bn = model.createResource();

      uri.addProperty(GLYTOUCAN.protein_db, bn);

      bn.addProperty(RDFS.seeAlso, model.createResource(UNIPROT_PREFIX + db.getId()));
      bn.addProperty(DCTerms.identifier, db.getId());

      EntityPolyType ept = EntityPolyType.lookUp(db.getType());
      if (ept != null) {
        bn.addProperty(RDF.type, GLYTOUCAN.forEntityPolyType(ept));
      }

      bn.addProperty(GLYTOUCAN.strand_id, db.getStrand_id());
      bn.addProperty(GLYTOUCAN.entity_id, db.getEntity_id());
      bn.addProperty(GLYTOUCAN.pdbx_seq_one_letter_code_can, db.getPdbx_seq_one_letter_code_can());
    }
  }

  private void addAr_pdbx_struct_mod_residue(Model model, Resource uri) {
    for (PdbxStructModResidue mr : entry.getAr_pdbx_struct_mod_residue()) {
      Resource node = model.createResource(
          String.format("https://rdf.wwpdb.org/pdb/%s/pdbx_struct_mod_residue/%s",
              entry.getPDB_id(), mr.getId()));

      uri.addProperty(PDBO.has_pdbx_struct_mod_residue, node);

      node.addProperty(RDF.type, PDBO.pdbx_struct_mod_residue);

      node.addProperty(PDBO.pdbx_struct_mod_residue_label_asym_id, mr.getLabel_asym_id());
      node.addProperty(PDBO.pdbx_struct_mod_residue_label_comp_id, mr.getLabel_comp_id());
      node.addProperty(PDBO.pdbx_struct_mod_residue_label_seq_id, mr.getLabel_seq_id());

      Optional.ofNullable(mr.getDetails()).ifPresent(x ->
          node.addProperty(PDBO.pdbx_struct_mod_residue_details, x)
      );
      Optional.ofNullable(mr.getParent_comp_id()).ifPresent(x ->
          node.addProperty(PDBO.pdbx_struct_mod_residue_parent_comp_id, x)
      );
    }
  }

  private void addGlycan(Model model, Resource uri) {
    for (GlycoEntry ge : entry.getGlyco_entries()) {
      Resource bn = model.createResource();

      uri.addProperty(GLYCAN.has_glycan, bn);

      bn.addProperty(RDF.type, GLYCAN.Saccharide);

      ge.getEntity_ids().forEach(x ->
          bn.addProperty(GLYTOUCAN.entity_id, x)
      );
      ge.getAsym_ids().forEach(x ->
          bn.addProperty(GLYTOUCAN.asym_id, x)
      );
      ge.getChem_comp_ids().forEach(x ->
          bn.addProperty(SIO.has_component_part, model.createResource(WWPDB_CC_PREFIX + x))
      );
      ge.getMod_residue_asym_ids().forEach(x ->
          bn.addProperty(GLYTOUCAN.mod_residue_asym_id, x)
      );
      ge.getPdbx_struct_mod_residue_ids().forEach(x ->
          bn.addProperty(GLYTOUCAN.pdbx_struct_mod_residue_id, x)
      );
      ge.getModels().forEach(x -> {
        Resource node = model.createResource();

        bn.addProperty(GLYTOUCAN.model, node);
        node.addProperty(GLYTOUCAN.pdbx_PDB_model_num, x.pdbx_PDB_model_num);
        node.addProperty(GLYTOUCAN.CTfile, x.CTfile);
      });
    }
  }

  private Model namespacedModel() {
    Model model = ModelFactory.createDefaultModel();

    PrefixMapping namespaces = PrefixMapping.Factory.create()
        .setNsPrefixes(PrefixMapping.Standard)
        .setNsPrefix("dcterms", DCTerms.getURI())
        .setNsPrefix("skos", SKOS.getURI())
        .setNsPrefix("sio", SIO.getURI())
        .setNsPrefix("glytoucan", GLYTOUCAN.getURI())
        .setNsPrefix("glycan", GLYCAN.getURI())
        .setNsPrefix("pdbo", PDBO.getURI())
        .setNsPrefix("uniprot", UNIPROT_PREFIX)
        .setNsPrefix("pdbcc", WWPDB_CC_PREFIX)
        .setNsPrefix("wwpdb", WWPDB_PREFIX);

    model.setNsPrefixes(namespaces);

    return model;
  }
}
