package org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class PDBO {
  public static final String uri = "https://rdf.wwpdb.org/schema/pdbx-v50.owl#";

  public static final Property has_pdbx_struct_mod_residue = PDBO.property("has_pdbx_struct_mod_residue");

  public static final Property pdbx_struct_mod_residue_auth_asym_id = PDBO.property("pdbx_struct_mod_residue.auth_asym_id");
  public static final Property pdbx_struct_mod_residue_auth_comp_id = PDBO.property("pdbx_struct_mod_residue.auth_comp_id");
  public static final Property pdbx_struct_mod_residue_auth_seq_id = PDBO.property("pdbx_struct_mod_residue.auth_seq_id");
  public static final Property pdbx_struct_mod_residue_details = PDBO.property("pdbx_struct_mod_residue.details");
  public static final Property pdbx_struct_mod_residue_id = PDBO.property("pdbx_struct_mod_residue.id");
  public static final Property pdbx_struct_mod_residue_label_asym_id = PDBO.property("pdbx_struct_mod_residue.label_asym_id");
  public static final Property pdbx_struct_mod_residue_label_comp_id = PDBO.property("pdbx_struct_mod_residue.label_comp_id");
  public static final Property pdbx_struct_mod_residue_label_seq_id = PDBO.property("pdbx_struct_mod_residue.label_seq_id");
  public static final Property pdbx_struct_mod_residue_parent_comp_id = PDBO.property("pdbx_struct_mod_residue.parent_comp_id");


  public static final Resource pdbx_struct_mod_residue = PDBO.resource("pdbx_struct_mod_residue");

  public static String getURI() {
    return uri;
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }
}
