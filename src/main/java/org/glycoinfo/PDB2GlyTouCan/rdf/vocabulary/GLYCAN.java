package org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class GLYCAN {
  public static final String uri = "http://purl.jp/bio/12/glyco/glycan#";

  public static final Property has_glycan = GLYCAN.property("has_glycan");

  public static final Resource Glycoprotein = GLYCAN.resource("Glycoprotein");
  public static final Resource Protein = GLYCAN.resource("Protein");
  public static final Resource Saccharide = GLYCAN.resource("Saccharide");

  public static String getURI() {
    return uri;
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }
}
