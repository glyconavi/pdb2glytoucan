package org.glycoinfo.PDB2GlyTouCan.rdf.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.glycoinfo.PDB2GlyTouCan.structure.EntityPolyType;

public class GLYTOUCAN {
  public static final String uri = "http://glycoinfo.org/ontology#";

  public static final Property atom_site_label_asym_id = GLYTOUCAN.property("atom_site_label_asym_id");
  public static final Property protein_db = GLYTOUCAN.property("protein_db");
  public static final Property strand_id = GLYTOUCAN.property("strand_id");
  public static final Property entity_id = GLYTOUCAN.property("entity_id");
  public static final Property asym_id = GLYTOUCAN.property("asym_id");
  public static final Property pdbx_seq_one_letter_code_can = GLYTOUCAN.property("pdbx_seq_one_letter_code_can");
  public static final Property mod_residue_asym_id = GLYTOUCAN.property("mod_residue_asym_id");
  public static final Property pdbx_struct_mod_residue_id = GLYTOUCAN.property("pdbx_struct_mod_residue_id");
  public static final Property pdbx_PDB_model_num = GLYTOUCAN.property("pdbx_PDB_model_num");
  public static final Property model = GLYTOUCAN.property("model");
  public static final Property CTfile = GLYTOUCAN.property("CTfile");

  public static String getURI() {
    return uri;
  }

  public static Resource forEntityPolyType(EntityPolyType ept) {
    String className = "UnknownType";

    switch (ept) {
      case CYCLIC_PSEUDO_PEPTIDE:
        className = "CyclicPseudoPeptide";
        break;
      case OTHER:
        className = "Other";
        break;
      case PEPTIDE_NUCLEIC_ACID:
        className = "PeptideNucleicAcid";
        break;
      case POLYDEOXYRIBONUCLEOTIDE:
        className = "Polydeoxyribonucleotide";
        break;
      case POLYDEOXYRIBONUCLEOTIDE_POLYRIBONUCLEOTIDE_HYBRID:
        className = "PolydeoxyribonucleotideOrPolyribonucleotideHybrid";
        break;
      case POLYPEPTIDE_D:
        className = "PolypeptideD";
        break;
      case POLYPEPTIDE_L:
        className = "PolypeptideL";
        break;
      case POLYRIBONUCLEOTIDE:
        className = "Polyribonucleotide";
        break;
      case POLYSACCHARIDE_D:
        className = "PolysaccharideD";
        break;
      case POLYSACCHARIDE_L:
        className = "PolysaccharideL";
        break;
    }

    return resource(className);
  }

  public static final Resource resource(String local) {
    return ResourceFactory.createResource(getURI() + local);
  }

  protected static final Property property(String local) {
    return ResourceFactory.createProperty(getURI(), local);
  }
}
