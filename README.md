# PDB2GlyTouCan

* **This repository has stopped updating. In the future, please use the following repository.**
    * https://gitlab.com/glyconavi/pdb2glycan

* This software outputs the result of analysis of glycan information about PDB's mmJSON format data.

## Requirements
* Operating system: macOS (Mac OS X) / Linux
* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher


## Build
```
$ mvn clean compile assembly:single
```

### OUTPUT PATH
```
target/PDB2GlyTouCan-[version number].jar
```

## OPTIONS
```
$ java -jar <JAR FILE>
[--no-network --pdb <DIRECTORY> --cc <DIRECTORY>]
[--output <DIRECTORY>]
--format <FORMAT>
<PDB_4_LETTER_CODE>
```
| option | argument | description |
| ------ | ------ | ------ |
| --cc | {DIRECTORY}  | Path to the directory where PDB Chemical Component Dictionary is placed |
| -f,--format | {FORMAT} | Output format {json,turtle(ttl)} |
| -h,--help |  | Show usage help |
| --no-network | None | Use local data, both of --cc {DIRECTORY} and --pdb {DIRECTORY} are required |
| -o, --output | {DIRECTORY}  | Path to output directory |
| --pdb | {DIRECTORY}  | Path to the directory where PDB Chemical Component Model getData file is placed |
| -v,--version |  | Print version information |


